<?php

class StoreController extends BaseController {

    public function showCatalog()
    {

        $allAlbums = Item::paginate(20);
        return View::make('store.catalog',array('allAlbums'=>$allAlbums,'cart'=>Cart::content()));
    }

    public function addAlbum()
    {
        $value = Input::get('id');
        $album = Item::find(intval($value));
        $discounts = DB::table('promotions')
            ->where('end', '>=', date('Y-m-d'))
            ->where('begin', '<=', date('Y-m-d'))
            ->join('item_promotion', 'promotions.id', '=', 'item_promotion.promotion_id')
            ->join('items', 'items.id', '=', 'item_promotion.item_id')
            ->join('authors', 'items.author_id', '=', 'authors.id')
            ->select(DB::raw('items.id as item_id, items.name as item_name, authors.name as author_name, items.cover_url as cover_url, items.price, promotions.discount as discount, begin, end'))
            ;
        foreach ($discounts->get() as $sale) {

        if(strcmp($sale->item_name,$album->name)==0){

        Cart::add($album->id, $album->name, 1, $album->price*(1-($sale->discount/100)) , array('type' => $album->type, 'author' => $album->author, 'discount' => $sale->discount, 'genre' => $album->genre));
        return Cart::total();
        }
        }
        Cart::add($album->id, $album->name, 1, $album->price , array('type' => $album->type, 'author' => $album->author, 'discount' => '0', 'genre' => $album->genre));
        return Cart::total();

    }

    public function checkstocks(){
        $response = array();
        $rows=Input::get('rowid_id');
        for($i=0;$i<sizeof($rows);$i++){
            $rowitem=explode(" ", $rows[$i]);
            $id=$rowitem[0];
            $rowid=$rowitem[1];
            try{
                $item = Item::find($id);
                $itemcart = Cart::get(Cart::search(array('id' => $id))[0]);
                $quantity= $itemcart->qty;
                $stock=$item->stock;
                if($stock<$quantity)
                    array_push($response,$rowid." ".$stock);
            }catch(Exception $e){
                Log::error('Error on the function checkstocks: ', $e->getMessage());
                return 'Caught exception: '.  $e->getMessage(). "\n";
            }

        }
        return $response;
    }

    public function suggestions(){
    $errors="";
    $containerTopAlbums= array();
    $response=array();
    $tags = array();
    $total=0;
    if(Cart::count() >0){


        foreach (Cart::content() as $album):
            $AlbumInfoQuery = "http://ws.audioscrobbler.com/2.0/?method=album.getTopTags&album="
                . urlencode($album->name) ."&artist=".urlencode($album->options->author->name)."&api_key=9e5978ffe340045b07f2420dc925e3ee";
            try{
            $AlbumInfoQueryContent = file_get_contents($AlbumInfoQuery);
            if ($AlbumInfoQueryContent){
                $xmlDocAlbumInfo = new DOMDocument('1.0', 'ISO-8859-1');
                $xmlDocAlbumInfo->loadXML($AlbumInfoQueryContent);
                    $tagsAlbum=$xmlDocAlbumInfo->getElementsByTagName("toptags")->item(0)->getElementsByTagName('tag');
                    foreach ($tagsAlbum as $tag) {
                        $imageAlbum=$tag->childNodes->item(1)->nodeValue;
                        array_push($tags,$imageAlbum);
                    }

            }else
                Log::error('The first query to the last fm wasn\'t successful: ');
            }catch(Exception $e){
                Log::error('Invalid tag on the first query to the last fm: ',  $e->getMessage());
            }
        endforeach;
        if(sizeof($tags)>9){
            $rand_keys = array_rand($tags, 10);
            $total=10;
        }elseif(sizeof($tags)>0){
            $rand_keys = array_rand($tags, sizeof($tags));
            $total=sizeof($tags);
        }else{
            $total=0;
        }
        for($i=0;$i<$total;$i++){
            $AlbumInfoQuery = "http://ws.audioscrobbler.com/2.0/?method=tag.getTopAlbums&tag="
                . urlencode($tags[$rand_keys[$i]])."&limit=50&api_key=9e5978ffe340045b07f2420dc925e3ee";
            try{
            $AlbumInfoQueryContent = file_get_contents($AlbumInfoQuery);
            if ($AlbumInfoQueryContent){
                $xmlDocAlbumInfo = new DOMDocument('1.0', 'ISO-8859-1');
                $xmlDocAlbumInfo->loadXML($AlbumInfoQueryContent);

                    $topalbums=$xmlDocAlbumInfo->getElementsByTagName("topalbums")->item(0)->getElementsByTagName('album');
                    foreach ($topalbums as $topalbum) {
                        $top=$topalbum->childNodes->item(1)->nodeValue;
                        array_push($containerTopAlbums,$top);
                    }

            }else
                Log::error('The second query to the last fm wasn\'t successful: ');
            }catch(Exception $e){
                Log::error('Invalid album on the second query to the last fm: ',  $e->getMessage());
            }
        }
    }
    $uniquecontainer = array_values(array_unique($containerTopAlbums));
    $howmany=0;

           $listItems="";
           foreach (Item::all() as $user)
                {
                    for($p=0;$p<sizeof($uniquecontainer);$p++){
                        if(strcmp($user->name,$uniquecontainer[$p])==0){
                            $howmany++;
                            //echo "<br>Comparing ".$user->name." with ". $uniquecontainer[$p]."<br>";

                            $listItems.= "<li><img src=\"".$user->cover_url."\">
                <div class=\"caption\" style=\" border-top-color: rgba(255, 255, 255, 0.35); border-top-style: groove; border-top-width: 1px;\">
                    ".$user->name."<br>
                    <small class=\"albumAuthor\">".$user->author->name."</small></div></li>";


                        }
                    }


                }
        $additionalInfo = "For curiosity<br>We found ".sizeof($tags)." tags</br>Searching for".$total."</br>There are ". sizeof($uniquecontainer)." suggestions available</br>There are currently ". $howmany." of those suggestions available in the store</br>";
        array_push($response,$listItems);
        array_push($response,$additionalInfo);
        return $response;
/*
 *  Uncomment to see all the albums
 * foreach($uniquecontainer as $topalbumm){
    echo $topalbumm."</br>";
}*/
    }

    public function suggestionsString()
    {
        $errors = "";
        $response = array();
        $containerAlbums = array();
        $containerAuthors = array();
        $album = Input::get('str');
        $howmany = 0;

        $AlbumInfoQuery = "http://ws.audioscrobbler.com/2.0/?method=album.search&album="
            . urlencode($album) . "&limit=100&api_key=9e5978ffe340045b07f2420dc925e3ee";
        try {
        $AlbumInfoQueryContent = file_get_contents($AlbumInfoQuery);
        if ($AlbumInfoQueryContent) {
            $xmlDocAlbumInfo = new DOMDocument('1.0', 'ISO-8859-1');
            $xmlDocAlbumInfo->loadXML($AlbumInfoQueryContent);

                $topalbums = $xmlDocAlbumInfo->getElementsByTagName("albummatches")->item(0)->getElementsByTagName('album');
                foreach ($topalbums as $topalbum) {
                    $albumNode = $topalbum->childNodes->item(1)->nodeValue;
                    $authorNode = $topalbum->childNodes->item(3)->nodeValue;
                    array_push($containerAlbums, $albumNode);
                    array_push($containerAuthors, $authorNode);
                }

        }else
            Log::error('The first query to the last fm wasn\'t successful: ');
        } catch (Exception $e) {
        Log::error('Invalid album and/or author on the first query to the last fm: ',  $e->getMessage());
        }

        $containerAlbumsQ2 = array();
        $AlbumInfoQuery = "http://ws.audioscrobbler.com/2.0/?method=tag.getTopAlbums&tag="
            . urlencode($album) . "&limit=100&api_key=9e5978ffe340045b07f2420dc925e3ee";
        try {
        $AlbumInfoQueryContent = file_get_contents($AlbumInfoQuery);
        if ($AlbumInfoQueryContent) {
            $xmlDocAlbumInfo = new DOMDocument('1.0', 'ISO-8859-1');
            $xmlDocAlbumInfo->loadXML($AlbumInfoQueryContent);

                $topalbums = $xmlDocAlbumInfo->getElementsByTagName("topalbums")->item(0)->getElementsByTagName('album');
                foreach ($topalbums as $topalbum) {
                    $albumNode = $topalbum->childNodes->item(1)->nodeValue;
                    array_push($containerAlbumsQ2, $albumNode);
                }

        }else
            Log::error('The second query to the last fm wasn\'t successful: ');
        } catch (Exception $e) {
        Log::error('Invalid album on the second query to the last fm: ',  $e->getMessage());
        }

        $listItems = "";
        foreach (Item::all() as $user) {
            for ($p = 0; $p < sizeof($containerAlbums); $p++) {
                if (strcmp($user->name, $containerAlbums[$p]) == 0 && strcmp($user->author->name, $containerAuthors[$p]) == 0) {
                    $howmany++;

                    $listItems .= "<li><img src=\"" . $user->cover_url . "\">
                <div class=\"caption\" style=\" border-top-color: rgba(255, 255, 255, 0.35); border-top-style: groove; border-top-width: 1px;\">
                    " . $user->name . "<br>
                    <small class=\"albumAuthor\">" . $user->author->name . "</small></div></li>";
                }
            }
            for ($p = 0; $p < sizeof($containerAlbumsQ2); $p++) {
                if (strcmp($user->name, $containerAlbumsQ2[$p]) == 0) {
                    $howmany++;

                    $listItems .= "<li><img src=\"" . $user->cover_url . "\">
                <div class=\"caption\" style=\" border-top-color: rgba(255, 255, 255, 0.35); border-top-style: groove; border-top-width: 1px;\">
                    " . $user->name . "<br>
                    <small class=\"albumAuthor\">" . $user->author->name . "</small></div></li>";
                }
            }
        }

        $additionalInfo = "For curious ones <br>Searching for " . $album . "</br>There are " . sizeof($containerAlbums)+sizeof($containerAlbumsQ2) . " suggestions available</br>There are currently " . $howmany . " of those suggestions available in the store</br>";
        array_push($response, $listItems);
        array_push($response, $additionalInfo);
        return $response;


        ?>

        <!-- Second Carousel with suggestions -->
        <?php


        /*
         *  Uncomment to see all the albums
         * foreach($uniquecontainer as $topalbumm){
            echo $topalbumm."</br>";
        }*/
    }


}
