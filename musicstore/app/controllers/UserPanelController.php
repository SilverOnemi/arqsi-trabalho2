<?php

/**
 * CatalogController Class
 *
 * Implements actions regarding user management
 */
class UserPanelController extends Controller
{
    /**
     * Displays the UserPanel page
     *
     * @return  Illuminate\Http\Response
     */
    public function showDashboard()
    {
        return View::make('userpanel/dashboard');
    }

}
