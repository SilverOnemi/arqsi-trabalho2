<?php
/**
 * Created by IntelliJ IDEA.
 * User: silverone
 * Date: 12/1/14
 * Time: 10:50 PM
 */

class SaleNotifierController extends BaseController{

    /**
     * Notifies Observers.
     * @param $order - orders with items as in: $order->belongsToMany('Item')->withPivot('quantity','price');
     */
    public function notifySale($order){
        //=Order::with('items')->first();
        $serializedOrder=$this->serializeOrder($order);

        $client = new GuzzleHttp\Client();
        $response = $client->post('http://uvm012.dei.isep.ipp.pt/importmusic/index.php/orders',
                                    ['future' => true,
                                     'json'=>$serializedOrder,
                                    ]
                                  );
        $response->then(
                function ($response) {
                    if($response->getStatusCode() != 200){
                        Log::warning('SaleNotifierController: status code:'.
                            $response->getStatusCode().' '.
                            $response->getReasonPhrase());
                    }
                },
                function ($error) {
                    Log::error('SaleNotifierController@notifySale: '.$error->getMessage());
                }
        );
    }

    private function serializeOrder($order){
        return [
            "store"=>"musicstore",
            "country"=>$order->country,
            "city"=>$order->city,
            "total"=>$order->total,
            "quantity"=>$order->quantity,
            "items"=>$this->serializeItems($order),
        ];
    }

    private function serializeItems($order){
        $orderItems=$order->items;
        $serializedItems=[];

        foreach($orderItems as $item){
            $serializedItems[]=[
                "author"=>$item->author->name,
                "name"=>$item->name,
                "genre"=>$item->genre,
                "type"=>$item->type,
                "quantity"=>$item->pivot->quantity,
                "price"=>$item->pivot->price,
            ];
        }

        return $serializedItems;
    }
}