<?php

class CartController extends BaseController {

    public function showCart()
    {
        return View::make('store.cart',array('cart'=>Cart::content()));
    }

    public function showCheckout()
    {
        return View::make('store.checkout',array('cart'=>Cart::content()));
    }

    public function addAlbum()
    {

        $value= Input::get('price');
        return $value;
    }

    public function addSingleAlbum($id)
    {

        $album = Item::find(intval($id));
        $discounts = DB::table('promotions')
            ->where('end', '>=', date('Y-m-d'))
            ->where('begin', '<=', date('Y-m-d'))
            ->join('item_promotion', 'promotions.id', '=', 'item_promotion.promotion_id')
            ->join('items', 'items.id', '=', 'item_promotion.item_id')
            ->join('authors', 'items.author_id', '=', 'authors.id')
            ->select(DB::raw('items.id as item_id, items.name as item_name, authors.name as author_name, items.cover_url as cover_url, items.price, promotions.discount as discount, begin, end'))
        ;
        foreach ($discounts->get() as $sale) {

            if(strcmp($sale->item_name,$album->name)==0){

                Cart::add($album->id, $album->name, 1, $album->price*(1-($sale->discount/100)) , array('type' => $album->type, 'author' => $album->author, 'discount' => $sale->discount, 'genre' => $album->genre));
                return Redirect::to('store/cart');
            }
        }
        Cart::add($album->id, $album->name, 1, $album->price , array('type' => $album->type, 'author' => $album->author, 'discount' => '0', 'genre' => $album->genre));
        return Redirect::to('store/cart');
    }

    public function clear(){
        Cart::destroy();
        return Redirect::to('store/cart');
    }

    public function update($id){
        try{
            $item = Item::find($id);
            if($item->stock<Input::get('quantity'))
                return "Error ".$item->stock;
            Cart::update(Cart::search(array('id' => $id))[0],intval(Input::get('quantity')));
        }catch(Exception $e){
            return 'Caught exception: '.  $e->getMessage(). "\n";
        }

        return Cart::total();

    }

    public function delete($id){
        try{
            Cart::remove(Cart::search(array('id' => $id))[0]);
        }catch(Exception $e){
            return 'Caught exception: '.  $e->getMessage(). "\n";
        }

        return Cart::total();
    }

    public function checkout(){

        $order = new Order;
        $order->user_id=2;
        $order->country=Input::get('country');
        $order->city=Input::get('city');
        $order->address=Input::get('address');
        $order->postal_code=Input::get('zipcode');
        $order->quantity=1;
        $total=0;
        foreach(Cart::content() as $row) :
            $total+=$row->price*$row->qty;
        endforeach;

        $order->total=$total;
        $order->save();

        foreach(Cart::content() as $row) :
            $order->items()->attach($row->id,array('quantity'=>$row->qty, 'price'=>$row->price));
            $item = Item::find($row->id);
            $item->stock=$item->stock-$row->qty;
            $item->save();
        endforeach;

        //saves the order and all its relations
        $order->push();

        try {
            $SaleNotifierController = new SaleNotifierController;
            $SaleNotifierController->notifySale($order);
        } catch (Exception $e) {
                Log::error('Cart Controller Notification Exception: ',  $e->getMessage());
        }
        return 1;
    }

    public function receipt(){
        $i=0;
        $items = array();
        foreach(Cart::content() as $row){
         array_push($items,array($i,$row->name,$row->options->author["name"],$row->qty,$row->options->type,$row->price,"None",$row->subtotal));
        $i++;
        }
        Cart::destroy();
return View::make('store.success',array('items'=>$items));
    }

}
