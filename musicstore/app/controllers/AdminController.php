<?php

class AdminController extends BaseController {

    public function showRestock()
    {
        $ctrl = new EditorServicesController('http://wvm012.dei.isep.ipp.pt/IDEIMusic/IDEIService.svc?WSDL','Q34UWU1PL34RPENB009Z9G8V');
        $json=$ctrl->getCatalog();
        return View::make('admin.restock',array('items'=>$json));
    }

    public function addItem($id)
    {
        $quantity=intval(Input::get('quantity'));
        $name = Input::get('name');
        $artist = Input::get('artist');
        $genre = Input::get('genre');
        $type = Input::get('type');
        $price = floatval (Input::get('price'));
        $cover_url = Input::get('cover_url');

        $ctrl = new EditorServicesController('http://wvm012.dei.isep.ipp.pt/IDEIMusic/IDEIService.svc?WSDL','Q34UWU1PL34RPENB009Z9G8V');
        $data = [["albumID"=>intval($id),"quantity"=>$quantity]];
        $json=$ctrl->orderAlbums($data);

        $items = Item::where('name', 'LIKE', $name);
        $count = $items->count();
        if($count){
            $item=$items->get()[0];
            $stock=$item->stock;
            $item->stock=$stock+$quantity;
            $item->save();
        }else{
            $item = new Item;
            $authors=Author::where('name','LIKE',$artist);
            $count = $authors->count();
            if($count){
                $author=$authors->get()[0];
                $item->author_id=$author->id;
            }else{
                $author = new Author;
                $author->name=$artist;
                $author->save();
                $item->author_id=$author->id;
            }
            $item->name=$name;
            $item->genre=$genre;
            $item->type=$type;
            $item->price=$price;
            $item->stock=$quantity;
            $item->cover_url=$cover_url;
            $item->save();
        }

        return $quantity;
    }

    public function showDashboard()
    {
        $data=[
        'item_count'=> DB::table('items')
            ->select(DB::RAW('count(*) as count'))
            ->remember(10)
            ->first(),

        'item_added_year'=> DB::table('items')
            ->select(DB::RAW('count(*) as count'))
            ->whereRaw('year(created_at) = year(NOW())')
            ->remember(10)
            ->first(),

         'item_added_month' => DB::table('items')
            ->select(DB::RAW('count(*) as count'))
            ->whereRaw('month(created_at) = month(NOW())')
            ->remember(10)
            ->first(),

            'author_count'=> DB::table('authors')
                ->select(DB::RAW('count(*) as count'))
                ->remember(10)
                ->first(),

            'user_count'=> DB::table('users')
                ->select(DB::RAW('count(*) as count'))
                ->remember(10)
                ->first(5),

            'user_unvalidated_count'=> DB::table('users')
                ->select(DB::RAW('count(*) as count'))
                ->where('confirmed','=','false')
                ->remember(10)
                ->first(),

            'user_registered_month'=> DB::table('users')
                ->select(DB::RAW('count(*) as count'))
                ->whereRaw('month(created_at) = month(NOW())')
                ->remember(10)
                ->first(),

            'user_invalidated_month'=> DB::table('users')
                ->select(DB::RAW('count(*) as count'))
                ->where('confirmed','=','false')
                ->whereRaw('month(created_at) < month(NOW())')
                ->remember(10)
                ->first(),

            'total_revenue'=> DB::table('orders')
            ->select(DB::RAW('sum(total) as revenue'))
            ->remember(10)
            ->first(),

            'total_revenue_month'=> DB::table('orders')
            ->select(DB::RAW('sum(total) as revenue'))
            ->whereRaw('month(created_at) = month(NOW())')
            ->remember(10)
            ->first(),

             'total_revenue_today' => DB::table('orders')
            ->select(DB::RAW('count(*) as count'))
            ->whereRaw('date(created_at) = date(NOW())')
            ->remember(10)
            ->first(),

            'order_average_revenue' => DB::table('orders')
                ->select(DB::RAW('avg(total) as average'))
                ->remember(10)
                ->first(),

            'total_orders' => DB::table('orders')
                ->select(DB::RAW('count(*) as count'))
                ->remember(10)
                ->first(),

            'total_orders_month' => DB::table('orders')
                ->select(DB::RAW('count(*) as count'))
                ->whereRaw('month(created_at) = month(NOW())')
                ->remember(10)
                ->first(),

            'total_orders_today' => DB::table('orders')
                ->select(DB::RAW('count(*) as count'))
                ->whereRaw('date(created_at) = date(NOW())')
                ->remember(10)
                ->first(),

            'order_average_quantity' => DB::table('orders')
                ->select(DB::RAW('avg(quantity) as average'))
                ->remember(10)
                ->first(),
        ];

        return View::make('admin.dashboard')->with($data);
    }

    public function showStatisticsOrders(){
        return View::make('admin.orders');
    }

    public function showStatisticsProducts(){
        return View::make('admin.products');
    }

    public function showStatisticsTopProducts(){
        return View::make('admin.topproducts');
    }
}
