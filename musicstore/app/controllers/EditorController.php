<?php

class EditorController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		try{
			$response = [
				'editors'  => [],
				'status_code' => 200,
			];

				$editors = Editor::all();
				$response['editors'][] =$editors;
		}catch (Exception $e){
			$response['status_code'] = 400;
			$response['error']=$e->getMessage();
			Log::error('EditorController@index: '.$e->getMessage());
		}finally{
			return Response::json($response);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$editor = Editor::find($id);

			$response = [
				'editor'  => $editor,
				'status_code' => 200,
			];
		}catch (Exception $e){
			$response['status_code'] = 404;
		}finally{
			return Response::json($response);
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		try{
			$response = [
				'status_code' => 200,
			];

			DB::transaction(function()
			{
				$name = Input::get('name');
				$service_url = Input::get('service_url');
				$api_key = Input::get('api_key');

				$store = new Editor;
				$store->name=$name;
				$store->service_url=$service_url;
				$store->api_key=$api_key;
				$store->save();
			});
		}catch (Exception $e){
			$response['status_code'] = 400;
			$response['error']= $e->getMessage();
			Log::info('OrderControler@store: '.$e->getMessage());
		}finally{
			return Response::json($response);
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		try{
			$response = [
				'status_code' => 200,
			];

			echo $id;

			DB::transaction(function($id) use ($id)
			{
				$editor = Editor::find($id);

				if(Input::has('name')){
					$editor->name=$name = Input::get('name');
				}
				if(Input::has('service_url')){
					$editor->service_url=Input::get('service_url');
				}
				if(Input::has('api_key')){
					$editor->api_key=Input::get('api_key');
				}

				$editor->save();
			});
		}catch (Exception $e){
			$response['status_code'] = 404;
			$response['error']= $e->getMessage();
			Log::info('OrderControler@store: '.$e->getMessage());
		}finally{
			return Response::json($response);
		}
	}
}
