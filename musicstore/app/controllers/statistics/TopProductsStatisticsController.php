<?php
/**
 * Created by IntelliJ IDEA.
 * User: silverone
 * Date: 12/3/14
 * Time: 8:33 PM
 */

class TopProductsStatisticsController extends Controller{
    public function getToday(){
        return DB::table('item_order')
            ->join('orders', 'item_order.order_id', '=', 'orders.id')
            ->join('items', 'item_order.item_id', '=', 'items.id')
            ->select(DB::raw('items.id as item_id, items.name as item_name,
                         sum(item_order.quantity) as quantity, sum(item_order.price) as price'))
            ->whereRaw('date(orders.created_at) = date(NOW())')
            ->groupBy('items.id')
            ->orderBy('quantity')->orderBy('price')
            ->take(10)
            ->remember(10)
            ->get();
    }

    public function getWeek(){
        return DB::table('item_order')
            ->join('orders', 'item_order.order_id', '=', 'orders.id')
            ->join('items', 'item_order.item_id', '=', 'items.id')
            ->select(DB::raw('items.id as item_id, items.name as item_name,
                         sum(item_order.quantity) as quantity, sum(item_order.price) as price'))
            ->whereRaw('date(orders.created_at) >= '.date('Y-m-d', strtotime('-7 days')))
            ->groupBy('items.id')
            ->orderBy('quantity')->orderBy('price')
            ->take(10)
            ->remember(10)
            ->get();
    }

    public function getMonth(){
        return DB::table('item_order')
            ->join('orders', 'item_order.order_id', '=', 'orders.id')
            ->join('items', 'item_order.item_id', '=', 'items.id')
            ->select(DB::raw('items.id as item_id, items.name as item_name,
                         sum(item_order.quantity) as quantity, sum(item_order.price) as price'))
            ->whereRaw('month(orders.created_at) = '.date('n'))
            ->groupBy('items.id')
            ->orderBy('quantity')->orderBy('price')
            ->take(10)
            ->remember(10)
            ->get();
    }

    public function getYear(){
        return DB::table('item_order')
            ->join('orders', 'item_order.order_id', '=', 'orders.id')
            ->join('items', 'item_order.item_id', '=', 'items.id')
            ->select(DB::raw('items.id as item_id, items.name as item_name ,
                         sum(item_order.quantity) as quantity, sum(item_order.price) as price'))
            ->groupBy('items.id')
            ->orderBy('quantity')->orderBy('price')
            ->take(10)
            ->remember(10)
            ->get();
    }
}