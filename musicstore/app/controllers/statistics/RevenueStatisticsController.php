<?php
/**
 * Created by IntelliJ IDEA.
 * User: silverone
 * Date: 12/4/14
 * Time: 9:47 PM
 */

class RevenueStatisticsController extends Controller {
    public function getMonthlyRevenue(){
        return DB::table('orders')->select(DB::raw('month(created_at) as month, sum(total) as revenue'))
            ->whereRaw('year(created_at) = year(NOW())')
            ->groupBy('month')
            ->orderBy('month')
            ->remember(10)
            ->get();
    }
}