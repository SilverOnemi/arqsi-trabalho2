<?php
/**
 * Created by IntelliJ IDEA.
 * User: silverone
 * Date: 12/3/14
 * Time: 5:32 PM
 */

class OrderStatisticsController extends Controller {
    public function getToday(){
        return DB::table('orders')->select(DB::raw('hour(created_at) as hour, count(*) as count'))
            ->whereRaw('date(created_at) = date(NOW())')
            ->groupBy('hour')
            ->orderBy('hour')
            ->remember(10)
            ->get();
    }

    public function getWeek(){
        return DB::table('orders')->select(DB::raw('day(created_at) as day, count(*) as count'))
            ->whereRaw('date(created_at) >= '. date('Y-m-d', strtotime('-7 days')))
            ->groupBy('day')->orderBy('day')
            ->remember(10)
            ->get();
    }

    public function getMonth(){
        return DB::table('orders')->select(DB::raw('month(created_at) as month, count(*) as count'))
            ->whereRaw('month(created_at) = '.date('n'))
            ->groupBy('month')->orderBy('month')
            ->remember(10)
            ->get();
    }

    public function getYear(){
        return DB::table('orders')->select(DB::raw('date(created_at) as date, year(created_at) as year
                                                    , month(created_at) as month, count(*) as count'))
            ->groupBy('year')->groupBy('month')
            ->orderBy('year')->groupBy('month')
            ->remember(10)
            ->get();
    }
}