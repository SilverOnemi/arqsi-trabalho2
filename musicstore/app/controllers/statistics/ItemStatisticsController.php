<?php
/**
 * Created by IntelliJ IDEA.
 * User: silverone
 * Date: 12/4/14
 * Time: 6:47 PM
 */

class ItemStatisticsController extends Controller{
    public function getStock(){
        $instock = DB::table('items')
            ->select(DB::raw('count(*) as in_stock'))
            ->whereRaw('stock > 0')
            ->remember(10)
            ->first();

        $nostock = DB::table('items')
            ->select(DB::raw('count(*) as out_of_stock'))
            ->whereRaw('stock <= 0')
            ->remember(10)
            ->first();
        return ['in_stock' =>$instock->in_stock,'out_of_stock' => $nostock->out_of_stock];
    }

    public function getTopAuthors(){
        return DB::table('item_order')
            ->join('items', 'item_order.item_id', '=', 'items.id')
            ->join('authors', 'items.author_id', '=', 'authors.id')
            ->select(DB::raw('authors.id as authors_id, authors.name, sum(item_order.quantity) as quantity'))
            ->groupBy('authors_id')
            ->orderBy('quantity')
            ->take(5)
            ->remember(10)
            ->get();
    }

    public function getTopItems(){
        return DB::table('item_order')
            ->join('items', 'item_order.item_id', '=', 'items.id')
            ->join('authors', 'items.author_id', '=', 'authors.id')
            ->select(DB::raw('items.id as item_id, items.name as item_name, authors.id as authors_id,
                             authors.name as author_name, sum(item_order.quantity) as quantity'))
            ->groupBy('item_id')
            ->orderBy('quantity')
            ->take(5)
            ->remember(10)
            ->get();
    }

}