<?php
/**
 * Created by IntelliJ IDEA.
 * User: silverone
 * Date: 12/3/14
 * Time: 10:39 PM
 */

class ProductStatisticsController extends Controller {
    public function getToday(){
        return DB::table('item_order')
            ->join('orders', 'item_order.order_id', '=', 'orders.id')
            ->select(DB::raw('hour(orders.created_at) as hour, sum(item_order.quantity) as quantity, sum(item_order.price) as total'))
            ->whereRaw('date(orders.created_at) = date(NOW())')
            ->groupBy('hour')
            ->remember(10)
            ->get();
    }

    public function getWeek(){
        return DB::table('item_order')
            ->join('orders', 'item_order.order_id', '=', 'orders.id')
            ->select(DB::raw('day(orders.created_at) as day, sum(item_order.quantity) as quantity, sum(item_order.price) as total'))
            ->whereRaw('date(orders.created_at) >= '.date('Y-m-d', strtotime('-7 days')))
            ->groupBy('day')
            ->orderBy('day')
            ->remember(10)
            ->get();
    }

    public function getMonth(){
        return DB::table('item_order')
            ->join('orders', 'item_order.order_id', '=', 'orders.id')
            ->select(DB::raw('month(orders.created_at) as month, sum(item_order.quantity) as quantity, sum(item_order.price) as total'))
            ->whereRaw('month(orders.created_at) = '.date('n'))
            ->groupBy('month')
            ->orderBy('month')
            ->remember(10)
            ->get();
    }

    public function getYear(){
        return DB::table('item_order')
            ->join('orders', 'item_order.order_id', '=', 'orders.id')
            ->select(DB::raw('date(orders.created_at) as date, sum(item_order.quantity) as quantity, sum(item_order.price) as total'))
            ->groupBy('date')
            ->orderBy('date')
            ->remember(10)
            ->get();
    }
}