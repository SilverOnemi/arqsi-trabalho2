<?php

class AlbumController extends BaseController {

    public function showAlbum($id)
    {
        $item = Item::find($id);
        return View::make('store.album',array(
            'id'=>$id,
            'name' =>$item->name,
            'price' =>$item->price,
            'stock' =>$item->stock,
            'genre' =>$item->genre,
            'author' =>$item->author->name,
            'type'=>$item->type,
            'imagesrc'=>$item->cover_url));
    }

    public function addAlbum()
    {

        $value= Input::get('price');
        return $value;
    }

}
