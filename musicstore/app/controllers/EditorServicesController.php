<?php
/**
 * Created by IntelliJ IDEA.
 * User: silverone
 * Date: 12/5/14
 * Time: 11:30 PM
 */

class EditorServicesController {
    private $url;
    private $api_key;
    private $client;

    function __construct($purl, $papi_key) {
        $this->url=$purl;
        $this->api_key=$papi_key;
        $this->client= new SoapClient($this->url,array("trace" => 1,"exceptions" => 0));
    }

    /**
     * Gets the catalog
     * @return mixed servers response with  the format:
     * [{"albumID":1,"title":"Ghost Stories","artistName":"Coldplay","genre":"Pop","type":"CD",
     * "coverURL":"http://upload.wikimedia.org/wikipedia/en/8/8a/Coldplay_-_Ghost_Stories.png",
     * "price":16.00,"stock":8},...]
     */
    public function getCatalog(){
        return $this->client->getCatalogo(array('api_key'=>$this->api_key));
    }

    public function showRestock()
    {
       return  $this->client->getCatalogo(array('api_key'=>$this->api_key));

    }

    /**
     * Send a new Order to the editor
     * @param $items [["albumID" => $item->albumID, "quantity"=>$item->quantity]]
     * @return mixed servers response
     */
    public function orderAlbums($items){
        return $this->client->sendOrder(array('api_key'=>$this->api_key,'order'=> json_encode($items)));
    }

    /**
     * Send a sale
     * @param $Order : Eloquent Order
     * @return mixed servers response
     */
    public function sendOrder($order){
        $items = $order->items()->get();
        $store_sales=[];

        foreach($items as $item){
            $store_sales[]=['name'=>$item->name, 'quantity'=>$item->pivot->quantity, 'price'=>floatval($item->pivot->price)];
        }

        $store_sales=json_encode($store_sales);
        return $this->client->ReceiveSales(array('api_key'=>$this->api_key, 'store_sales'=>$store_sales));
    }

}