<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		$data=['bestselling'=>DB::table('item_order')
			->join('items', 'item_order.item_id', '=', 'items.id')
			->join('authors', 'items.author_id', '=', 'authors.id')
			->select(DB::raw('items.id as item_id, items.name as item_name, authors.id as authors_id,
                             authors.name as author_name, sum(item_order.quantity) as quantity, cover_url'))
			->groupBy('item_id')
			->orderBy('quantity')
			->take(6)
			->remember(10)
			->get(),

			'latest'=>DB::table('items')
				->join('authors', 'items.author_id', '=', 'authors.id')
				->select(DB::raw('items.id as item_id, items.name as item_name, authors.id as authors_id,
                             authors.name as author_name, cover_url'))
				->orderBy('created_at','DESC')
				->take(6)
				->remember(10)
				->get(6),

			/*'sales'=>Promotion::where('begin','<=',date('Y-m-d'))
				->where('end','>=',date('Y-m-d'))
				->where('begin','<=',date('Y-m-d'))
				->with('items')
				->orderBy(DB::RAW('RAND()'))
				->remember(10)
				->get(),*/

			'sales'=>DB::table('promotions')
				->where('end','>=',date('Y-m-d'))
				->where('begin','<=',date('Y-m-d'))
				->join('item_promotion', 'promotions.id', '=', 'item_promotion.promotion_id')
				->join('items', 'items.id', '=', 'item_promotion.item_id')
				->join('authors', 'items.author_id', '=', 'authors.id')
				->select(DB::raw('items.id as item_id, items.name as item_name, authors.name as author_name, items.cover_url as cover_url, items.price, promotions.discount as discount, begin, end'))
				->orderBy(DB::raw('RAND()'))
				->take(6)
				->remember(2)
				->get(),
			];

		return View::make('home')->with($data);
	}

	public function test(){
		$ctrl = new EditorServicesController('http://wvm012.dei.isep.ipp.pt/IDEIMusic/IDEIService.svc?WSDL','Q34UWU1PL34RPENB009Z9G8V');
		$item=$ctrl->getCatalog();
		return $item;
	}

}
