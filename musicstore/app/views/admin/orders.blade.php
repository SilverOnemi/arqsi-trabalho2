@extends('admin.master')

@section('title')
Order Statistics
@stop

@section('header')
Order Statistics
@stop

@section('dashcontent')
<div class="row">
     <div class="col-xs-6">
	    <div id="today" style="width:500px;height:300px;"></div>
	 </div>
     <div class="col-xs-6">
	    <div id="week" style="width:500px;height:300px;"></div>
	 </div>
 </div>
 <div class="row">
      <div class="col-xs-6">
	 <div id="months" style="width:500px;height:300px;"></div>
 	 </div>
      <div class="col-xs-6">
	 <div id="years" style="width:500px;height:300px;"></div>
 	 </div>
 </div>
@stop

@section('otherScripts')
{{ HTML::script('js/jquery-2.1.1.min.js') }}
{{ HTML::script('js/flot/excanvas.min.js') }}
{{ HTML::script('js/flot/jquery.flot.js') }}
{{ HTML::script('js/flot/jquery.flot.time.min.js') }}

<script>
$(document).ready(function(){

        $.getJSON("{{URL::to('statistics/orders/today');}}", function(json) {
             var d1=insertZeroedDays(json,'hour');

             var data = [ {
                label: "Today",
                data: d1,
                lines: {
                    how: true,
                    fill: true,
                    color: '#0066FF'
                },
                points: {
                    show: true,
                    fillColor: '#0066FF'
                },
                color: '#0066FF'
                }];

             $.plot($("#today"), data,{
                 yaxis: {
                      tickDecimals: 0
                 },
                 xaxis: {
                      min:0,
                      max:24.9
                 },

                 "lines": {"show": "true"},
                 "points": {"show": "true"},
                 clickable:true,hoverable: true
              });
         });


         $.getJSON("{{URL::to('statistics/orders/week');}}", function(json) {
             var d1=insertZeroedDays(json,'day');

             var data = [ {
                label: "Last 7 days",
                data: d1,
                lines: {
                    how: true,
                    fill: true,
                    color: '#33CC33'
                },
                 points: {
                     show: true,
                     fillColor: '#33CC33'
                 },
                 color: '#33CC33'
             }];

             $.plot($("#week"), data,{
                 yaxis: {
                    tickDecimals: 0
                 },
                 xaxis: {
                    tickDecimals: 0
                 },

                 "lines": {"show": "true"},
                 "points": {"show": "true"},
                 clickable:true,hoverable: true
             });
        });

        $.getJSON("{{URL::to('statistics/orders/month');}}", function(json) {
             var d1=insertZeroedDays(json,'month');

            var data = [ {
                label: "Orders per Month",
                data: d1,
                lines: {
                    how: true,
                    fill: true
                },
                points: {
                    show: true
                }
            }];

            $.plot($("#months"), data,{
               yaxis: {
                tickDecimals: 0
               },
               xaxis: {
               min:0.5,
               max:12.5
               },

               "lines": {"show": "true"},
               "points": {"show": "true"},
               clickable:true,hoverable: true
            });
        });

         $.getJSON("{{URL::to('statistics/orders/years');}}", function(json) {
               var d1=[];
               for(key in json)
                   d1.push([new Date(json[key].date).getTime(), json[key].count]);

               var data = [ {
                   label: "Orders per Year",
                   data: d1
                   /*bars: {
                       show: true,
                       align: "center",
                       barWidth: 12*24*60*60*1000,
                       fill: true,
                       lineWidth: 1
                   }*/,
                   lines: {
                       show: true,
                       fill: true,
                       color: '#AA4643'
                   },
                   points: {
                       show: true,
                       fillColor: '#AA4643'
                   },
                   color: '#AA4643'
                   //        yaxis: 2
               }];

               $.plot($("#years"), data,{
                   yaxis: {
                    tickDecimals: 0
                   },
                   xaxis: {
                     mode: "time",
                     timeformat: "%b"
                   },

                   "lines": {"show": "true"},
                   "points": {"show": "true"},
                   clickable:true,hoverable: true
               });
         });
 });

 function insertZeroedDays(data, column) {
    if(data.length==0){
    return data
    }
   var newData = [[data[0][column],data[0].count]];

   for (var i = 1; i < data.length; i++) {
     var diff =  data[i][column]-data[i - 1][column];
     if (diff > 1) {
       var startDate = data[i - 1][column];
       for (var j = 0; j < diff - 1; j++) {
         var fillDate = parseInt(startDate)+j+1;
         newData.push([fillDate, 0]);
       }
     }
     newData.push([data[i][column], data[i].count]);
   }
   return newData;
 }
</script>
@stop
