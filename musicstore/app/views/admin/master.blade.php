@extends('layouts.master')

@section('others')
{{ HTML::style('css/dashboard.css') }}
@stop

@section('content')
{{Route::currentRouteName();}}
@stop
@section('othercontent')
<div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
          @if(Route::getCurrentRoute()->getPath()=='admin')
          <li class="active">
          @else
          <li>
          @endif
            <a href="{{URL::to('admin') }}">Overview <span class="sr-only">(current)</span></a></li>
          </ul>
          <ul class="nav nav-sidebar">
              @if(Route::getCurrentRoute()->getPath()=='admin/restock')
                <li class="active">
              @else
                <li>
             @endif
             <a href="{{URL::to('admin/restock')}}">Restock</a></li>
          </ul>
          <a>Statistics</a>
          <ul class="nav nav-sidebar">
            @if(Route::getCurrentRoute()->getPath()=='admin/statistics/orders')
              <li class="active">
            @else
              <li>
            @endif
            <a href="{{URL::to('admin/statistics/orders')}}">Orders</a></li>
            @if(Route::getCurrentRoute()->getPath()=='admin/statistics/products')
              <li class="active">
            @else
              <li>
            @endif
            <a href="{{URL::to('admin/statistics/products')}}">Products</a></li>
            @if(Route::getCurrentRoute()->getPath()=='admin/statistics/topproducts')
               <li class="active">
            @else
               <li>
            @endif
            <a href="{{URL::to('admin/statistics/topproducts')}}">Top Products</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">
              @yield('header')
          </h1>
          <div class="row placeholders">
             @yield('dashcontent')
          </div>
        </div>
      </div>
</div>
@stop
