@extends('admin.master')

@section('title')
Admin Dashboard
@stop

@section('header')
Dashboard
@stop

@section('others')
@parent
{{ HTML::style('css/bootstrap-extensions.css') }}
{{ HTML::style('css/dataTables.bootstrap.css') }}
@stop
@section('otherScripts')
@parent
{{ HTML::script('js/jquery-2.1.1.min.js')   }}
{{ HTML::script('js/jquery.bootpag.min.js') }}
{{ HTML::script('js/jquery.dataTables.min.js') }}
{{ HTML::script('js/dataTables.bootstrap.js') }}
<script>

    $(document).ready(function() {
        $(".add").click(function (event) {
            var id = parseInt($(this).text());
            quantity = document.getElementById("quantity"+id).value;
            stock = document.getElementById("stock" + id).innerText;
            name = document.getElementById("name" + id).innerText;
            genre = document.getElementById("genre" + id).innerText;
            artist = document.getElementById("artist" + id).innerText;
            price = document.getElementById("price" + id).innerText;
            price = price.substring(0, price.length - 1);
            type = document.getElementById("type" + id).innerText;
            cover_url = document.getElementById("cover_url" + id).innerText;
            if(parseInt(quantity)>parseInt(stock)){
                var nodeList = document.getElementsByClassName("label");
                for (var i = 0; i < nodeList.length; i++) {
                    var item = nodeList[i];
                    item.style.display = "none";
                }
                document.getElementById("warning" + id).style.display = "inline";
                document.getElementById("warning" + id).innerHTML = "Only " + stock + " in stock";
            }else
            $.post(
                $(this).attr('to'),
                {
                    "id":id,
                    "quantity": quantity,
                    "name":name,
                    "price": price,
                    "genre": genre,
                    "artist": artist,
                    "type": type,
                    "cover_url" : cover_url
                },
                function (data) {
                    var nodeList = document.getElementsByClassName("label");
                    for (var i = 0; i < nodeList.length; i++) {
                        var item = nodeList[i];
                        item.style.display = "none";
                    }
                    if (data.indexOf("Error ") > -1) {
                        var array = data.split(" ");
                        document.getElementById("warning" + id).style.display = "inline";
                        document.getElementById("warning" + id).innerHTML = "Only " + array[1] + " in stock";
                    } else {
                        document.getElementById("success" + id).style.display = "inline";
                        document.getElementById("stock" + id).innerText=parseInt(stock)-parseInt(data);
                    }
                }
            );

        });
    });
$(document).ready(function() {
$('#example').dataTable( {
    "lengthMenu": [[-1,10, 25, 50], ["All",10, 25, 50]]
} );
} );

</script>
<script
    src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.0/angular.min.js"></script>

@section('dashcontent')

<div  class="container" style="padding-top: 50px;">
    <table ng-app="" id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Name</th>
            <th>Author</th>
            <th>Genre</th>
            <th>Stock</th>
            <th>Quantity</th>
            <th>Type</th>
            <th>Price</th>
            <th>Total</th>
        </tr>
        </thead>

        <tbody>

        <?php $i=0;
        $jsond=json_decode($items->getCatalogoResult);
        $size=sizeof($jsond);

        for($i=0;$i<$size;$i++) {
            $albumid=$jsond[$i]->albumID;
            $title=$jsond[$i]->title;
            $genre=$jsond[$i]->genre;
            $artist=$jsond[$i]->artistName;
            $coverURL=$jsond[$i]->coverURL;
            $price=$jsond[$i]->price;
            $type=$jsond[$i]->type;
            $stock=$jsond[$i]->stock;
            ?>

            <tr class="itemdata" rowid="<?php echo $albumid;?>" ng-init="qty<?php echo $albumid;?>=<?php echo 1;?>;price<?php echo $albumid;?>=<?php echo $price;?>">
                <td id="name<?php echo $i;?>"><?php echo $title;?></td>
                <td id="artist<?php echo $i;?>"><?php echo $artist;?></td>
                <td id="genre<?php echo $i;?>"><?php echo $genre;?></td>
                <td id="stock<?php echo $i;?>"><?php echo $stock; ?></td>

                <td><form class="form-inline">
                        <input id="quantity<?php echo $i;?>" class="form-control" type="text" ng-model="qty<?php echo $albumid;?>" value="<?php echo 1;?>">
                        <a to="restock/additem/<?php echo $albumid;?>" class="btn btn-primary add"><div  class="rowId" style="display:none;"><?php echo $i; ?></div><i class="glyphicon glyphicon-plus"></i></a>
                        <span id="success<?php echo $i;?>" class="label label-success" style="display:none;">Ordered successfully</span>
                        <span id="warning<?php echo $i;?>" class="label label-warning" style="display:none;">Only 23 in stock</span>
                    </form>
                </td>
                <td id="type<?php echo $i;?>"><?php echo $type;?></td>
                <td id="price<?php echo $i;?>" ng-model="price<?php echo $albumid;?>"><?php echo $price;?>€</td>
                <td >{{ qty<?php echo $albumid;?> * price<?php echo $albumid;?> }}€<span id="cover_url<?php echo $i;?>" style="display: none;"><?php echo $coverURL;?></span></td>

            </tr>
            <?php }?>
        </tbody>
    </table>

</div>

@stop