@extends('admin.master')

@section('title')
Product Statistics
@stop

@section('header')
Product Statistics
@stop

@section('dashcontent')
<div class="row">
     <div class="col-xs-6">
	    <div id="today" style="width:500px;height:300px;"></div>
	 </div>
     <div class="col-xs-6">
	    <div id="week" style="width:500px;height:300px;"></div>
	 </div>
 </div>
 <div class="row">
      <div class="col-xs-6">
	 <div id="months" style="width:500px;height:300px;"></div>
 	 </div>
      <div class="col-xs-6">
	 <div id="years" style="width:500px;height:300px;"></div>
 	 </div>
 </div>
@stop

@section('otherScripts')
{{ HTML::script('js/jquery-2.1.1.min.js') }}
{{ HTML::script('js/flot/excanvas.min.js') }}
{{ HTML::script('js/flot/jquery.flot.js') }}
{{ HTML::script('js/flot/jquery.flot.time.min.js') }}

<script>
$(document).ready(function(){

        $.getJSON("{{URL::to('statistics/products/today');}}", function(json) {
             var d1=insertZeroedDays(json,'hour','quantity');
             var d2=insertZeroedDays(json,'hour','total');

             var data = [ {
                label: "Items sold Today",
                data: d1,
                lines: {
                    how: true,
                    fill: true,
                    color: '#0066FF'
                },
                points: {
                    show: true,
                    fillColor: '#0066FF'
                },
                color: '#0066FF'
                },
                {
                label: "Revenue today",
                data: d2,
                lines: {
                     how: true,
                     fill: true,
                     color: '#FF3030'
                },
                points: {
                     show: true,
                     fillColor: '#FF3030'
                 },
                    color: '#FF3030'
                 }
                ];

             $.plot($("#today"), data,{
                 yaxis: {
                      tickDecimals: 0
                 },
                 xaxis: {
                      min:0,
                      max:24
                 },
                 "lines": {"show": "true"},
                 "points": {"show": "true"},
                 clickable:true,hoverable: true
              });
         });


         $.getJSON("{{URL::to('statistics/products/week');}}", function(json) {
             var d1=insertZeroedDays(json,'day','quantity');
             var d2=insertZeroedDays(json,'day','total');

             var data = [ {
                label: "Items sold past 7 days",
                data: d1,
                lines: {
                    how: true,
                    fill: true,
                    color: '#228B22'
                },
                 points: {
                     show: true,
                     fillColor: '#228B22'
                 },
                 color: '#228B22'
             },
             {
                label: "Revenue total past 7 days",
                data: d2,
                lines: {
                     how: true,
                     fill: true,
                     color: '#CC7722'
                },
                points: {
                     show: true,
                     fillColor: '#CC7722'
                },
                     color: '#CC7722'
                }
              ];

             $.plot($("#week"), data,{
                 yaxis: {
                    tickDecimals: 0
                 },
                 xaxis: {
                    tickDecimals: 0
                 },

                 "lines": {"show": "true"},
                 "points": {"show": "true"},
                 clickable:true,hoverable: true
             });
        });

        $.getJSON("{{URL::to('statistics/products/month');}}", function(json) {
             var d1=insertZeroedDays(json,'month','quantity');
             var d2=insertZeroedDays(json,'month','total');

            var data = [ {
                label: "items sold per Month",
                data: d1,
                lines: {
                    how: true,
                    fill: true,
                    color: '#DA70D6'
                },
                points: {
                    show: true,
                  fillColor: '#DA70D6'
                },
                color: '#DA70D6'
            },
            {
            label: "Revenue total per Month",
            data: d2,
            lines: {
                  how: true,
                  fill: true,
                  color: '#FFE600'
            },
            points: {
                  show: true,
                  fillColor: '#FFE600'
            },
                   color: '#FFE600'
            }
            ];

            $.plot($("#months"), data,{
               yaxis: {
                tickDecimals: 0
               },
               xaxis: {
               min:0.5,
               max:12.5
               },

               "lines": {"show": "true"},
               "points": {"show": "true"},
               clickable:true,hoverable: true
            });
        });

         $.getJSON("{{URL::to('statistics/products/year');}}", function(json) {
               var d1=[];
               for(key in json)
                   d1.push([new Date(json[key].date).getTime(), json[key]['quantity']]);

               var d2=[];
               for(key in json)
                   d2.push([new Date(json[key].date).getTime(), json[key]['total']]);

               var data = [ {
                   label: "Items sold per Year",
                   data: d1,
                   lines: {
                       show: true,
                       fill: true,
                       color: '#AA4643'
                   },
                   points: {
                       show: true,
                       fillColor: '#AA4643'
                   },
                   color: '#AA4643'
                   //        yaxis: 2
               },
               {
                   label: "Revenue total per year",
                   data: d2,
                   lines: {
                       how: true,
                       fill: true,
                       color: '#FFE600'
                   },
                   points: {
                        show: true,
                        fillColor: '#FFE600'
                   },
                   color: '#FFE600'
               }
               ];
               $.plot($("#years"), data,{
                   yaxis: {
                    tickDecimals: 0
                   },
                   xaxis: {
                     mode: "time",
                     timeformat: "%b"
                   },

                   "lines": {"show": "true"},
                   "points": {"show": "true"},
                   clickable:true,hoverable: true
               });
         });
 });

 function insertZeroedDays(data, column, valuecolumn) {
   if(data.length==0){
         return data
   }

   var newData = [[data[0][column],data[0][valuecolumn]]];

   for (var i = 1; i < data.length; i++) {
     var diff =  data[i][column]-data[i - 1][column];
     if (diff > 1) {
       var startDate = data[i - 1][column];
       for (var j = 0; j < diff - 1; j++) {
         var fillDate = parseInt(startDate)+j+1;
         newData.push([fillDate, 0]);
       }
     }
     newData.push([data[i][column], data[i][valuecolumn]]);
   }
   return newData;
 }
</script>
@stop
