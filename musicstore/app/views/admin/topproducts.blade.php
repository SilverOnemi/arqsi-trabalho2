@extends('admin.master')

@section('title')
Product Statistics
@stop

@section('header')
Top Product Statistics
@stop

@section('dashcontent')
<div class="row">
     <div class="col-xs-6 placeholder">
	    <div id="today" style="width:540px;height:300px;"></div>
	 </div>
     <div class="col-xs-6 placeholder">
	    <div id="week" style="width:540px;height:300px;"></div>
	 </div>
 </div>
 <div class="row">
      <div class="col-xs-6 placeholder">
	 <div id="months" style="width:540px;height:300px;"></div>
 	 </div>
      <div class="col-xs-6 placeholder">
	 <div id="years" style="width:540px;height:300px;"></div>
 	 </div>
 </div>
@stop

@section('otherScripts')
{{ HTML::script('js/jquery-2.1.1.min.js') }}
{{ HTML::script('js/flot/excanvas.min.js') }}
{{ HTML::script('js/flot/jquery.flot.js') }}
{{ HTML::script('js/flot/jquery.flot.categories.js') }}

<script>
$(document).ready(function(){

          $.getJSON("{{URL::to('statistics/topproducts/today');}}", function(json) {
                var d1=[];
                for(key in json)
                    d1.push([json[key].item_name,json[key].quantity]);

                var d2=[];
                for(key in json)
                    d2.push([json[key].item_name,json[key].price]);

                var data = [ {
                    label: "Daily Item Quantity",
                    data: d1,
                    bars: {
                        show: true,
                        fill: true,
                        color: '#24db2d'
                    },
                    color: '#24db2d'
                },
                {
                   label: "Daily Item revenue",
                   data: d2,
                   lines: {
                          show: true,
                          color: '#c94736'
                   },
                   points: {
                          show: true
                   },
                           color: '#c94736'
                   }
                ];
                $.plot($("#today"), data,{
                    yaxis: {
                    axisLabel: "Sales",
                    tickDecimals: 0
                    },
                    xaxis: {
                     axisLabel: "Name",
                     mode: "categories"
                    },
                    bars: {
                        align: "center",
                        barWidth: 0.5
                    },

                    clickable:true,hoverable: true
                });
          });



            $.getJSON("{{URL::to('statistics/topproducts/week');}}", function(json) {
                       var d1=[];
                       for(key in json)
                           d1.push([json[key].item_name,json[key].quantity]);

                       var d2=[];
                       for(key in json)
                           d2.push([json[key].item_name,json[key].price]);

                       var data = [ {
                           label: "Weekly Item Sales",
                           data: d1,
                           bars: {
                               show: true,
                               fill: true,
                               color: '#4d79b3'
                           },
                           color: '#4d79b3'
                       },
                       {
                          label: "Weekly Item Revenue",
                          data: d2,
                          lines: {
                                 show: true,
                                 color: '#ff2424'
                          },
                          points: {
                                 show: true
                          },
                                  color: '#ff2424'
                          }
                       ];
                       $.plot($("#week"), data,{
                           yaxis: {
                           axisLabel: "Sales",
                           tickDecimals: 0
                           },
                           xaxis: {
                            axisLabel: "Name",
                            mode: "categories"
                           },
                           bars: {
                               align: "center",
                               barWidth: 0.5
                           },

                           clickable:true,hoverable: true
                       });
                 });


        $.getJSON("{{URL::to('statistics/topproducts/month');}}", function(json) {
                       var d1=[];
                       for(key in json)
                           d1.push([json[key].item_name,json[key].quantity]);

                       var d2=[];
                       for(key in json)
                           d2.push([json[key].item_name,json[key].price]);

                       var data = [ {
                           label: "Monthly Item sales",
                           data: d1,
                           bars: {
                               show: true,
                               fill: true,
                               color: '#d52a5a'
                           },
                           color: '#d52a5a'
                       },
                       {
                          label: "Monthly Item Revenue",
                          data: d2,
                          lines: {
                                 show: true,
                                 color: '#4ab583'
                          },
                          points: {
                                 show: true
                          },
                                  color: '#4ab583'
                          }
                       ];
                       $.plot($("#months"), data,{
                           yaxis: {
                           axisLabel: "Sales",
                           tickDecimals: 0
                           },
                           xaxis: {
                            axisLabel: "Name",
                            mode: "categories"
                           },
                           bars: {
                               align: "center",
                               barWidth: 0.5
                           },

                           clickable:true,hoverable: true
                       });
                 });

         $.getJSON("{{URL::to('statistics/topproducts/year');}}", function(json) {
               var d1=[];
               for(key in json)
                   d1.push([json[key].item_name,json[key].quantity]);

               var d2=[];
               for(key in json)
                   d2.push([json[key].item_name,json[key].price]);

               var data = [ {
                   label: "Annual Item Quantity",
                   data: d1,
                   bars: {
                       show: true,
                       fill: true,
                       color: '#f28907'
                   },
                   color: '#f28907'
               },
               {
                  label: "Annual Item revenue",
                  data: d2,
                  lines: {
                         show: true,
                         color: '#8d46b9'
                  },
                  points: {
                         show: true
                  },
                          color: '#8d46b9'
                  }
               ];
               $.plot($("#years"), data,{
                   yaxis: {
                   axisLabel: "Sales",
                   tickDecimals: 0
                   },
                   xaxis: {
                    axisLabel: "Name",
                    mode: "categories"
                   },
                   bars: {
                       align: "center",
                       barWidth: 0.5
                   },

                   clickable:true,hoverable: true
               });
         });
 });
</script>
@stop
