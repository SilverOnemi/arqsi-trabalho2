@extends('admin.master')

@section('title')
Admin Dashboard
@stop

@section('header')
Dashboard
@stop

@section('dashcontent')
<div class="placeholder">
<h2>Graphs</h2>
<div class="col-xs-6 col-sm-3 placeholder">
	 <div id="stock" style="width:350px;height:200px;"></div>
     <h4>Stock %</h4>
</div>

<div class="col-xs-6 col-sm-3 placeholder">
    <div id="author" style="width:350px;height:200px;"></div>
	<h4>Top Authors</h4>
</div>

<div class="col-xs-6 col-sm-3 placeholder">
    <div id="album" style="width:350px;height:200px;"></div>
	<h4>Top Albums</h4>
</div>

<div class="col-xs-6 col-sm-3 placeholder">
    <div id="monthlyRevenue" style="width:350px;height:200px;"></div>
	<h4>Monthly Revenue</h4>
</div>

<h2>Periodic Stats</h2>

</div>
<div class="row">
  <div class="col-xs-6 col-md-3">
  <h3>Orders</h3>
  <table class="table table-bordered">
    <tr>
      <td class="active">Total Orders:</td>
      <td class="success">{{$total_orders->count}}</td>
    </tr>
     <tr>
        <td class="active">This month:</td>
        <td class="success">{{$total_orders_month->count}}</td>
     </tr>
      <tr>
         <td class="active">Today:</td>
         <td class="success">{{$total_orders_today->count}}</td>
      </tr>
        <tr>
            <td class="active">Average Quantity:</td>
            <td class="success">{{$order_average_quantity->average}}</td>
        </tr>
    </table>
  </div>

<div class="col-xs-6 col-md-3">
  <h3>Revenue</h3>
  <table class="table table-bordered">
    <tr>
      <td class="active">Total Revenue:</td>
      <td class="success">{{$total_revenue->revenue}}</td>
    </tr>
     <tr>
        <td class="active">Revenue this month:</td>
        <td class="success">{{$total_revenue_month->revenue}}</td>
     </tr>
      <tr>
         <td class="active">Revenue today:</td>
         <td class="success">{{$total_revenue_today->count}}</td>
      </tr>
        <tr>
            <td class="active">Average Order Revenue:</td>
            <td class="success">{{$order_average_revenue->average}}</td>
        </tr>
    </table>
  </div>

  <div class="col-xs-6 col-md-3">
    <h3>Items</h3>
    <table class="table table-bordered">
      <tr>
        <td class="active">Total items:</td>
        <td class="success">{{$item_count->count}}</td>
      </tr>
       <tr>
          <td class="active">Items added this year:</td>
          <td class="success">{{$item_added_year->count}}</td>
       </tr>
        <tr>
           <td class="active">Items added this month:</td>
           <td class="success">{{$item_added_month->count}}</td>
        </tr>
         <tr>
           <td class="active">Total Authors</td>
           <td class="success">{{$author_count->count}}</td>
         </tr>
      </table>
    </div>

     <div class="col-xs-6 col-md-3">
        <h3>Users</h3>
        <table class="table table-bordered">
          <tr>
            <td class="active">Total:</td>
            <td class="success">{{$user_count->count}}</td>
          </tr>
           <tr>
              <td class="active">Unvalidated :</td>
              <td class="success">{{$user_unvalidated_count->count}}</td>
           </tr>
             <tr>
                <td class="active">Registered this Month:</td>
                <td class="success">{{$user_registered_month->count}}</td>
             </tr>
              <tr>
                 <td class="active">Unvalidated larger than a Month:</td>
                 <td class="success">{{$user_invalidated_month->count}}</td>
              </tr>
          </table>
        </div>

</div>
@stop

@section('otherScripts')
{{ HTML::script('js/jquery-2.1.1.min.js') }}
{{ HTML::script('js/flot/excanvas.min.js') }}
{{ HTML::script('js/flot/jquery.flot.js') }}
{{ HTML::script('js/flot/jquery.flot.pie.min.js') }}
<script>

 
$(document).ready(function () {
 $.getJSON("{{URL::to('statistics/items/stocks');}}", function(json) {
    var data = [
        { label: "Out of Stock",  data: json.out_of_stock, color: "#4572A7"},
        { label: "In Stock",  data: json.in_stock, color: "#80699B"}
    ];

     $.plot($("#stock"), data, {
         series: {
             pie: {
                 show: true,
                 innerRadius: 0.36,
                 combine: {
                     color: '#999',
                     threshold: 0.1
                 }
             }
         },
         grid: {
             hoverable: true
         },
         legend: {
            show:false
         }
     });
     //$("#stock").bind("plothover", pieHover);
});
 $.getJSON("{{URL::to('statistics/items/top/authors');}}", function(json) {
   var colors=['#4ab583','#4d79b3','#748a8b','#d52a5a','#8d46b9','#16100d'];
   var d1=[];
   for(var i = 0; i < json.length; i++)
        d1.push({label:json[i].name, data: json[i].quantity, color:colors[i]});

   $.plot($("#author"), d1, {
         series: {
             pie: {
                 innerRadius: 0.36,
                 show: true,
                 combine: {
                     color: '#999',
                     threshold: 0.1
                 }
             }
         },
         grid: {
             hoverable: true
         },
         legend: {
            show:false
         }
     });
    // $("#author").bind("plothover", pieHover);
});

$.getJSON("{{URL::to('statistics/items/top/items');}}", function(json) {
   var colors=['#c46b3b','#b54a7c','#3ec197','#84a0a4','#4db3a7','#a49a28'];
   var d1=[];
   for(var i = 0; i < json.length; i++)
        d1.push({label:json[i].item_name, data: json[i].quantity, color:colors[i]});

   $.plot($("#album"), d1, {
         series: {
             pie: {
                 innerRadius: 0.36,
                 show: true,
                 combine: {
                     color: '#999',
                     threshold: 0.1
                 }
             }
         },
         grid: {
             hoverable: true
         },
         legend: {
            show:false
         }
     });
     //$("#stock").bind("plothover", pieHover);
});

$.getJSON("{{URL::to('statistics/revenue/month');}}", function(json) {
   var colors=['#748a8b','#b54a7c','#3ec197','#84a0a4','#4db3a7','#a49a28','#a68608','#a68608','#8d46b9','90606b','9f8c1d','a37d5c'];
   var d1=[];
   for(var i = 0; i < json.length; i++)
      d1.push({label:json[i].month, data: json[i].revenue, color:colors[i]});

   $.plot($("#monthlyRevenue"), d1, {
         series: {
             pie: {
                 innerRadius: 0.36,
                 show: true,
                 combine: {
                     color: '#999',
                     threshold: 0.2
                 }
             }
         },
         grid: {
             hoverable: true
         },
         legend: {
            show:false
         }
     });
     //$("#monthlyRevenue").bind("plothover", pieHover);
});

});

function pieHover(event, pos, obj) {
     if (!obj)
         return;

     percent = parseFloat(obj.series.percent).toFixed(2);
     $("#pieHover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' ('+percent+'%)</span>');
 }

</script>
@stop