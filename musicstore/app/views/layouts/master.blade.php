<!DOCTYPE html>
<html>
    <head>
        <title>
            @section('title')
            Musicstore
            @show
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- CSS are placed here -->
        {{ HTML::style('css/bootstrap.css') }}
        {{ HTML::style('css/bootstrap-theme.css') }}

        <style>
        @section('styles')
            body {
                padding-top: 60px;
            }
        @show
        </style>

        <script type="text/javascript">
            @section('scripts')

            @show
        </script>
        @section('others')

        @show
    </head>
    <!-- onLoad="initialize();" -->
    <body vocab="http://schema.org/" >
        <!-- Navbar -->
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="#">Musicstore</a>
                </div>
                <!-- Everything you want hidden at 940px or less, place within here -->
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li><a href="{{{ URL::to('') }}}">Home</a></li>
                        <li>{{ HTML::link('store/catalog', 'Catalog') }}</li>
                        @if ( Auth::user() && Auth::user()->hasRole('admin') )
                            <li>{{ HTML::link('admin', 'Admin Dash') }}</li>
                        @endif
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        @if ( Auth::guest() )
                        <li>{{ HTML::link('users/create', 'Sign Up') }}</li>
                        <li>{{ HTML::link('users/login', 'Login') }}</li>
                        @else
                        <li>{{ HTML::link('store/cart', " ".Cart::total()."€",array('class'=>'glyphicon glyphicon-shopping-cart','id'=>'cartTotal')) }}</li>
                        <li>{{ HTML::link('users/logout', 'Logout') }}</li>
                        @endif
                    </ul>
                </div>
            </div>
        </div> 


        @section('othercontent')
        @show
        <!-- Container -->
        <div class="container">

            <!-- Success-Messages -->
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <h4>Success</h4>
                    {{{ $message }}}
                </div>
            @endif

            <!-- Content -->
            @yield('content')
  <!-- FOOTER -->
          <footer>
             <p class="pull-right"><a href="#">Back to top</a></p>
          </footer>
        </div>

        <!-- Scripts are placed here -->
        @section('otherScripts')
        @show
        {{ HTML::script('js/bootstrap.min.js') }}




    </body>
</html>
