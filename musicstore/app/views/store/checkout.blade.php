@extends('...layouts.master')

@section('title')
@parent
:: Secret
@stop

@section('others')
@parent
<!--<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet"> -->
{{ HTML::style('css/bootstrap-extensions.css') }}
{{ HTML::style('css/dataTables.bootstrap.css') }}
{{ HTML::style('css/bootstrap-formhelpers.min.css') }}
@stop
@section('otherScripts')
@parent
{{ HTML::script('js/jquery-2.1.1.min.js')   }}
{{ HTML::script('js/jquery.bootpag.min.js') }}
{{ HTML::script('js/jquery.dataTables.min.js') }}
{{ HTML::script('js/dataTables.bootstrap.js') }}
{{ HTML::script('js/bootstrap-formhelpers.min.js') }}
<script>
    $(document).ready(function() {
        $('#example').dataTable();
        $( ".btn-primary" ).click(function( event ) {
            var error=0;
            document.getElementsByClassName("bfh-selectbox-toggle")[1].style.borderColor="";document.getElementById("countriesW").style.display="none";
            document.getElementById("address").style.borderColor="";document.getElementById("addressW").style.display="none";
            document.getElementById("zipcode").style.borderColor="";document.getElementById("zipcodeW").style.display="none";

            if(document.getElementsByClassName("bfh-selectbox-option")[1].innerText==''){
            document.getElementsByClassName("bfh-selectbox-toggle")[1].style.borderColor="#a94442";document.getElementById("countriesW").style.display="inline";error=1;}
            if(document.getElementById("address").value=='')
            {document.getElementById("address").style.borderColor="#a94442";document.getElementById("addressW").style.display="inline";error=1;}
            if(document.getElementById("zipcode").value=='')
            {document.getElementById("zipcode").style.borderColor="#a94442";document.getElementById("zipcodeW").style.display="inline";error=1;}

            if(error==0){


            $.post(
                $( this ).prop( 'action' ),
                {
                    "country": document.getElementsByClassName("bfh-selectbox-option")[0].innerText,
                    "city": document.getElementsByClassName("bfh-selectbox-option")[1].innerText,
                    "address":document.getElementById("address").value,
                    "zipcode":document.getElementById("zipcode").value
                },
                function( data ) {
                    if(data==1){
                        window.location.replace("success?country="+document.getElementsByClassName("bfh-selectbox-option")[0].innerText+"&city="+document.getElementsByClassName("bfh-selectbox-option")[1].innerText+"&address="+document.getElementById("address").value+"&zipcode="+document.getElementById("zipcode").value);
                    }else{
                        //error
                    }

                }
            );
            }
        });

    });
</script>



@stop
@section('content')
<div class="container" style="margin-top: 50px;">
<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Name</th>
        <th>Author</th>
        <th>Quantity</th>
        <th>Type</th>
        <th>Price</th>
        <th>Discount</th>
        <th>Total</th>
    </tr>
    </thead>

    <tbody>

    <?php foreach($cart as $row) :?>

        <tr>
            <td><?php echo $row->name;?></td>
            <td><?php echo $row->options->author["name"];?></td>
            <td><?php echo $row->qty;?> </td>
            <td><?php echo $row->options->type;?></td>
            <td><?php echo $row->price/(1-($row->options->discount/100));?>€</td>
            <td><?php echo $row->options->discount; ?>%</td>
            <td><?php echo $row->subtotal;?>€</td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>
<div  style="margin-left:86%">
    <table>
        <tr>
            <td colspan="4" style="border: 0; width: 120px;"><strong>Total</strong></td>
            <td style="border: 0;"><?php echo Cart::total(); ?>€</td>
        </tr>
    </table>
</div>
</div>

<div class="container" style="width:600px;">
    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm text-left form-horizontal">
                    <fieldset style="padding-left: 30px;">
                        <legend class="text-center header">Required Information</legend>

                        <div class="form-group">
                            <div class="col-md-8">
                                <div id="countries_states2" class="bfh-selectbox bfh-countries" data-country="PT" data-flags="true">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8">
                                <div class="bfh-selectbox bfh-states" data-country="countries_states2" data-state="">
                                </div>
                                <small id="countriesW" style="color:#a94442;display: none;">Select a country</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input id="address" name="email" type="text" placeholder="Address" class="form-control">
                                <small id="addressW" style="color:#a94442;display: none;">The address is required</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input id="zipcode" name="zipcode" type="text" placeholder="Postal Code" class="form-control">
                                <small id="zipcodeW" style="color:#a94442;display: none;">The Postal code is required</small>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <a type="submit" class="btn btn-primary">Order</a>
                            </div>
                        </div>
                    </fieldset>
            </div>
        </div>
    </div>
</div>

@stop