@extends('...layouts.master')

@section('title')
@parent
:: Secret
@stop

@section('others')
@parent
<!--<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet"> -->
{{ HTML::style('css/bootstrap-extensions.css') }}
@stop
@section('otherScripts')
@parent
{{ HTML::script('js/jquery-2.1.1.min.js')   }}
{{ HTML::script('js/jquery.bootpag.min.js') }}
<script>
    $(document).ready(function() {
        $( ".btn-xxxlarge" ).click(function( event ) {



            $.post(
                $( this ).prop( 'action' ),
                {
                    "price": $(this).text()
                },
                function( data ) {
                    alert("Responde from server:"+data);
                }
            );

        });
    });
</script>

@stop
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <?php
            $AlbumInfoQuery = "http://ws.audioscrobbler.com/2.0/?method=album.getinfo&album="
            . urlencode($name) ."&artist=".urlencode($author)."&api_key=9e5978ffe340045b07f2420dc925e3ee";
            try{
            $AlbumInfoQueryContent = file_get_contents($AlbumInfoQuery);
            if ($AlbumInfoQueryContent){
            $xmlDocAlbumInfo = new DOMDocument('1.0', 'ISO-8859-1');
            $xmlDocAlbumInfo->loadXML($AlbumInfoQueryContent);
            $imageAlbum=$xmlDocAlbumInfo->getElementsByTagName("album")->item(0)->getElementsByTagName('image')->item(3)->childNodes->item(0);
                if(isset($imageAlbum->nodeValue)){
                    $imagesrc=$imageAlbum->nodeValue;
                }else{
                    $imagesrc="";
                }
            }else
                Log::error('Invalid album when searching for information on query to the last fm (album.blade.php): ');
            }catch(Exception $e){
                Log::error('Invalid album when searching for information on query to the last fm (album.blade.php): ');
            }
            ?>
            <img src={{ "$imagesrc" }}></div>
        <div class="col-md-8">

            <table cellspacing="0" width="100%" class="table table-hover" style="margin-top: 0px;">

                <tr>
                    <th style="width:20%;">Album Name</th><td>{{ $name }}</td>
                </tr>
                <tr>
                    <th style="width:20%;">Price</th><td>{{ $price }}€</td>
                </tr>
                <tr>
                    <th style="width:20%;">Genre</th><td>{{ $genre }}</td>
                </tr>
                <tr>
                    <th style="width:20%;">Stock</th><td>{{ $stock }}</td>
                </tr>
                <tr>
                    <th style="width:20%;">Author</th><td>{{ $author }}</td>
                </tr>
                <tr>
                    <th style="width:20%;">Type</th><td>{{ $type }}</td>
                </tr>
            </table>
            <!-- Indicates a successful or positive action -->
            <a href="../cart/{{$id}}" type="button" class="btn btn-success" style="left:80%; position: relative; padding-bottom: 5px; margin-bottom:30px; margin-top:-20px;">Add to Cart</a>
            <table cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th >Title</th>
                    <th >Length</th>
                    <th >Listen to it</th>
                </tr>
                </thead>
                <?php
                try{


                $nodeList=$xmlDocAlbumInfo->getElementsByTagName("track");
                for ($i=0;$i<$nodeList->length;$i++) {
                echo "<tr>";
                echo "<td>".$nodeList->item($i)->getElementsByTagName('name')->item(0)->childNodes->item(0)->nodeValue."</td>";
                echo "<td>".$nodeList->item($i)->getElementsByTagName('duration')->item(0)->childNodes->item(0)->nodeValue."</td>";
                echo "<td><a href=".$nodeList->item($i)->getElementsByTagName('url')->item(0)->childNodes->item(0)->nodeValue."><span class=\"label label-info\">Listen</span></a></td>";
                echo "</tr>";
                }
                }catch(Exception $e){
                    Log::error('Invalid album when searching for informationon the tracks on query to the last fm (album.blade.php): ');

                }

                ?>
            </table>


        </div>
    </div>
</div>



@stop