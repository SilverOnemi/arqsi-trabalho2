@extends('layouts.master')

@section('title')
@parent
:: Success
@stop

@section('script')
@stop

@section('others')
@parent
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>

<script type="text/javascript" src="js/jquery.simplePagination.js"></script>

<script  type="text/javascript" src="js/jquery.tooltipster.min.js"></script>
<script type="text/javascript" src="js/tabs.js"></script>
<script type="text/javascript" src="js/toptags.js"></script>
<script type="text/javascript" src="js/events.js"></script>
<script type="text/javascript" src="js/tracks.js"></script>
<script src="js/bootstrap.min.js"></script>
@stop

@section('content')
    <div class="container" style="padding-top: 50px; width:800px;">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title">Ordered Successfully</h3>
            </div>
            <div class="panel-body">
                The order has been completed with success.
                <table class="table table-striped" width="647">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Author</th>
                        <th>Quantity</th>
                        <th>Type</th>
                        <th>Price</th>
                        <th>Discount</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach($items as $item) :?>



                        <tr>
                            <td><?php echo $item[0];?></td>
                            <td><?php echo $item[1];?></td>
                            <td><?php echo $item[2];?></td>
                            <td><?php echo $item[3];?> </td>
                            <td><?php echo $item[4];?></td>
                            <td><?php echo $item[5];?>€</td>
                            <td><?php echo $item[6];?></td>
                            <td><?php echo $item[7];?>€</td>
                        </tr>


                        <?php endforeach;?>
                    </tbody>
                </table>
                <table style="width:400px;">
                    <tr><td><strong>Country</strong></td><td> <?php echo Input::get('country')?></td></tr>
                    <tr><td><strong>City</strong></td><td> <?php echo Input::get("city")?></td></tr>
                    <tr><td><strong>Address</strong></td><td> <?php echo Input::get("address")?></td></tr>
                    <tr><td><strong>Zip Code</strong></td><td> <?php echo Input::get("zipcode")?></td></tr>
                </table>
            </div>
        </div>
    </div>





@stop