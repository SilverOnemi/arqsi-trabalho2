@extends('...layouts.master')

@section('title')
@parent
:: Secret
@stop

@section('others')
@parent
<!--<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet"> -->
{{ HTML::style('css/bootstrap-extensions.css') }}
{{ HTML::style('css/dataTables.bootstrap.css') }}
@stop
@section('otherScripts')
@parent
{{ HTML::script('js/jquery-2.1.1.min.js')   }}
{{ HTML::script('js/jquery.bootpag.min.js') }}
{{ HTML::script('js/jquery.dataTables.min.js') }}
{{ HTML::script('js/dataTables.bootstrap.js') }}
<script>
    $(document).ready(function() {
        $( ".edit" ).click(function( event ) {
            var id=parseInt($(this).text());
            $.post(
                $( this ).attr( 'to' ),
                {
                    "quantity": document.getElementById("quantity"+id).value
                },
                function( data ) {

                    var nodeList = document.getElementsByClassName("label");
                    for(var i=0;i<nodeList.length;i++){
                        var item= nodeList[i];
                            item.style.display = "none";
                    }
                    if(data.indexOf("Error ") > -1){
                        var array = data.split(" ");
                        document.getElementById("warning"+id).style.display="inline";
                        document.getElementById("warning"+id).innerHTML = "Only "+array[1]+" in stock";
                    }else{
                        document.getElementById("success"+id).style.display="inline";
                        document.getElementById("cartTotal").innerHTML = " "+data+"€";
                        document.getElementById("totalPrice").innerHTML = " "+data+"€";
                    }
                }
            );

        });
        $( ".delete" ).click(function( event ) {
            var row=parseInt($(this).text());

            $.post(
                $( this ).attr( 'to' ),
                {
                    "quantity": document.getElementsByClassName("form-control")[2+row].value
                },
                function( data ) {
                    location.reload();
                    /* Its preferable to reload the page instead of chanching all of the id's of the items
                    document.getElementById("cartTotal").innerHTML = " "+data+"€";
                    document.getElementById("example").deleteRow(1+row);
                    for(var i=0,x=0;i<document.getElementsByClassName("rowId").length;i=i+2){
                        document.getElementsByClassName("rowId")[i].innerText=x;
                        document.getElementsByClassName("rowId")[i+1].innerText=x;
                        x++;
                    }*/

                }
            );

        });
        $( "#checkout" ).click(function( event ) {
            var rowid_id = [];
            $rows = document.getElementsByClassName("itemdata");

            for(var i=0;i<$rows.length;i++){
                rowid_id.push($rows[i].getAttribute("rowid")+" "+i);
            }


                $.post(
                    "checkout/verifystocks",
                    {
                        "rowid_id": rowid_id
                    },
                    function( data ) {
                        var nodeList = document.getElementsByClassName("label");
                        for(var i=0;i<nodeList.length;i++){
                            var item= nodeList[i];
                            item.style.display = "none";
                        }
                        if(data==""){
                            window.location.replace("checkout");
                        }else{
                            for(var i=0;i<data.length;i++){
                                rowitem=data[i].split(" ");
                                rowid=rowitem[0];
                                quantity=rowitem[1];
                                document.getElementById("warning"+rowid).style.display="inline";
                                document.getElementById("warning"+rowid).innerHTML = "Only "+quantity+" in stock";
                            }
                        }



                    }
                );
        });
    });
    $(document).ready(function() {
        $('#example').dataTable( {
            "lengthMenu": [[-1,10, 25, 50], ["All",10, 25, 50]]
        } );
    } );

</script>
<script
    src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.0/angular.min.js"></script>


@stop
@section('content')
<div  class="container" style="padding-top: 50px;">
<table ng-app="" id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>&nbsp;</th>
        <th>Name</th>
        <th>Author</th>
        <th>Genre</th>
        <th>Quantity</th>
        <th>Type</th>
        <th>Price</th>
        <th>Discount</th>
        <th>Total</th>
    </tr>
    </thead>

    <tbody>

    <?php $i=0; foreach($cart as $row) :?>

        <tr class="itemdata" rowid="<?php echo $row->id;?>" ng-init="qty<?php echo $row->id;?>=<?php echo $row->qty;?>;price<?php echo $row->id;?>=<?php echo $row->price;?>">
            <td><a href="album/{{$row->id;}}" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-info-sign"></i></a></td>
            <td><?php echo $row->name;?></td>
            <td><?php echo $row->options->author["name"];?></td>
            <td><?php echo $row->options->genre;?></td>

            <td><form class="form-inline">
                    <input id="quantity<?php echo $i;?>" class="form-control" type="text" ng-model="qty<?php echo $row->id;?>" value="<?php echo $row->qty;?>">
                    <a to="edit/<?php echo $row->id;?>" class="btn btn-default edit"><div  class="rowId" style="display:none;"><?php echo $i; ?></div><i class="glyphicon glyphicon-pencil"></i></a>
                    <a to="delete/<?php echo $row->id;?>" class="btn btn-primary delete"><div class="rowId" style="display:none;"><?php echo $i; ?></div><i class="glyphicon glyphicon-trash"></i></a>
                    <span id="success<?php echo $i;?>" class="label label-success" style="display:none;">Changed successfully</span>
                    <span id="warning<?php echo $i;?>" class="label label-warning" style="display:none;">Only 23 in stock</span>
                </form>
            </td>
            <td>
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                        <?php echo $row->options->type;?>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">CD</a></li>
                        <li role="presentation" class="disabled"><a role="menuitem" tabindex="-1" href="#">Vinil</a></li>
                    </ul>
                </div>
            </td>
            <td ng-model="price<?php echo $row->id;?>"><?php echo $row->price/(1-($row->options->discount/100));?>€</td>
            <td><?php echo $row->options->discount;?>%</td>
            <td>{{ qty<?php echo $row->id;?> * price<?php echo $row->id;?> }}€</td>
        </tr>
    <?php $i++; endforeach;?>
    </tbody>
</table>

<div  style="margin-left:86%">
<table>
    <tr>
        <td colspan="4" style="border: 0; width: 120px;">Without discount</td>
        <td style="border: 0;"><?php echo Cart::total(); ?>€</td>
    </tr>
    <tr>
        <td colspan="4" style="border: 0; width: 120px;"><strong>Total</strong></td>
        <td id="totalPrice" style="border: 0;"><?php echo Cart::total(); ?>€</td>
    </tr>
</table>

    </div>
<?php if ( sizeof($cart)!=0){ ?>
   <a href="clear" type="button" id="clearButton" class="btn btn-info" style="position:relative; left:55%;;" >Clear</a>
<?php }else{ ?>
    <a href="clear" type="button" id="clearButton" class="btn btn-info disabled" style="position:relative; left:55%;;" >Clear</a>
<?php } ?>
<?php if ( sizeof($cart)!=0){ ?>
    <a id="checkout"  type="button" id="clearButton" class="btn btn-success" style="position:relative; left:60%;;">Checkout</a>
<?php }else{ ?>
    <a id="checkout"  type="button" id="clearButton" class="btn btn-success disabled" style="position:relative; left:60%;;">Checkout</a>
<?php } ?>
</div>
@stop