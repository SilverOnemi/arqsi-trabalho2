@extends('...layouts.master')

@section('title')
@parent
:: Secret
@stop

@section('others')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<!-- Second carousel -->
{{ HTML::style('css/carousel.suggestions.css') }}
{{ HTML::script('js/jquery.jcarousel.js')   }}
{{ HTML::script('js/jcarousel.ajax.js')   }}
{{ HTML::script('js/jquery.jcarousel.min.js')   }}
@parent
<!--<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet"> -->
{{ HTML::style('css/bootstrap-extensions.css') }}
{{ HTML::style('css/loader.css') }}
@stop
@section('otherScripts')
@parent
{{ HTML::script('js/jquery-2.1.1.min.js')   }}
{{ HTML::script('js/jquery.bootpag.min.js') }}
<script>
    $(document).ready(function() {
        $( ".btn-xxxlarge" ).click(function( event ) {
            var array = $(this).text().split(" ");
            $.post(
                $( this ).prop( 'action' ),
                {
                    "id": array[0]
                },
                function( data ) {
                    document.getElementById("cartTotal").innerHTML = " "+data+"€";
                    getSuggestions();

                }
            );

        });

        getSuggestions();

    });
    function getSuggestions(){
        loading();
        $.post(
            "catalog/suggestions",
            {
            },
            function( data ) {
                show();
                document.getElementById("suggestions").innerHTML = data[0];
                document.getElementById("suggestionsContainerInfo").innerHTML = data[1];
            }
        );
    }
    function getSuggestionsString(str){
        loading();
        $.post(
            "catalog/suggestionsString",
            {
                "str" : str
            },
            function( data ) {
                show();
                document.getElementById("suggestions").innerHTML = data[0];
                document.getElementById("suggestionsContainerInfo").innerHTML = data[1];


            }
        );
    }
    function loading(){
        document.getElementById("suggestionsContainer").style.display = "none";
        document.getElementById("suggestionsContainerInfo").style.display = "none";
        document.getElementById("loader").style.display = "inline";
    }
    function show(){
        document.getElementById("suggestionsContainer").style.display = "inline";
        document.getElementById("suggestionsContainerInfo").style.display = "inline";
        document.getElementById("loader").style.display = "none";
    }
    function filter( value){
        var nodeList = document.getElementsByClassName("albumItem");

        var nodeList = document.getElementsByClassName("albumItem");
        for(var i=0;i<nodeList.length;i++){
            var item= nodeList[i];
            if(value==''){
                item.style.display="inline";
            }else if(item.childNodes[1].childNodes[3].childNodes[1].childNodes[0].nodeValue.indexOf(value) > -1){

                item.style.display="inline";
            }else if(item.childNodes[1].childNodes[3].childNodes[3].childNodes[0].textContent.indexOf(value) > -1){
                item.style.display="inline";
            }else
                item.style.display="none";

            //name (item.childNodes[1].childNodes[3].childNodes[1].childNodes[0].nodeValue);
            //author (item.childNodes[1].childNodes[3].childNodes[3].childNodes[0].textContent);
            //alert(item.childNodes[1].childNodes[3].childNodes[1].childNodes[0].nodeValue);

        }
        return value;
    }
</script>

@stop
@section('content')
<div class="container" style="margin-left: 65%;padding-bottom:20px;">
<div class="col-sm-6">
    <div id="example_filter" class="dataTables_filter"><label>Search:<input onkeyup="filter(this.value)" id="search" type="search"
                                                                            class="form-control input-sm"
                                                                            placeholder="" aria-controls="example"></label>
    </div>
</div>
</div>
<div class="container">
    <div id="portfolio" class="row">

        <?php foreach ($allAlbums as $album): ?>

        <!-- ITEM-->
        <div class="col-md-2 albumItem" style="height:300px;">
            <div class="thumbnail">
                <img src=<?php echo "\"".$album->cover_url."\""; if($album->type == 'Vinyl') echo "class=\"img-circle\""; else echo "class=\"img-rounded\""; ?> />
                <div class="caption" style=" border-top-color: rgba(255, 255, 255, 0.35); border-top-style: groove; border-top-width: 1px;">
                   <p><?php echo $album->name; ?></p>
                    <p><small class="albumAuthor"><?php echo $album->author->name; ?></small></p>
                    <div class="row">
                        <div class="col-md-6">
                            @if ( Auth::guest() )
                            <a class="btn btn-xxxlarge btn-primary" href="../users/create"><i class="glyphicon glyphicon-shopping-cart"></i><?php echo $album->price; ?>€</a>
                            @else
                            <a class="btn btn-xxxlarge btn-primary" ><i class="glyphicon glyphicon-shopping-cart"><div style="display:none;"><?php echo $album->id; ?></div></i> <?php echo $album->price; ?>€</a>
                            @endif                        </div>
                        <div class="col-md-6">
                            <a class="btn btn-xxxlarge btn-primary" href="album/<?php echo $album->id ?>"><i class="glyphicon glyphicon-info-sign"></i>  Info</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- / ITEM-->
        <?php endforeach; ?>
        <div class="container">
        <?php echo $allAlbums->links(); ?>
        </div>
    </div>
</div>
<label>Search:<input onkeyup="if (event.keyCode == 13){getSuggestionsString(this.value);}" id="search" type="search" class="form-control input-sm" placeholder="" aria-controls="example"></label>
<div id="loader"  >
    <div class='loader1'>
        <div>
            <div>
                <div>
                    <div>
                        <div>
                            <div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="suggestionsContainer"  class="container" >

    <!-- Second Carousel with suggestions -->
    <div class="wrapper" style="-webkit-box-sizing: initial;-moz-box-sizing: initial;box-sizing: initial;">

        <div class="jcarousel-wrapper">
            <div class="jcarousel" data-jcarousel="true">
                <ul id="suggestions" style="left: 0px; top: 0px;">

                </ul>
            </div>

            <a href="#" class="jcarousel-control-prev inactive" data-jcarouselcontrol="true">‹</a>
            <a href="#" class="jcarousel-control-next" data-jcarouselcontrol="true">›</a>
        </div>
    </div>

</div>
<span id="suggestionsContainerInfo" ></span>



@stop