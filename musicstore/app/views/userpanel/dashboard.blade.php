@extends('layouts.master')

@section('title')
@parent
User Panel
@stop

@section('styles')
@parent
        .maincontent {
            background-color: #FFF;
            margin: auto;
            padding: 20px;
            width: 400px;
            box-shadow: 0 0 20px #AAA;
            position: relative;
            top: 50%;
            transform: translateY(10%);
        }
@stop
@section('content')
<div class="maincontent">
        <h1>{{ Confide::user()->username }}</h1>
        <div class="well">
            <b>email:</b> {{ Confide::user()->email }}
        </div>
     </div>
@stop