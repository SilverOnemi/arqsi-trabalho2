@extends('layouts.master')

@section('title')
@parent
:: Home
@stop

@section('script')
<script type="text/javascript">
    $(window).load(function(){

        mapProp = {
            center: new google.maps.LatLng(41.177980, -8.608262),
            zoom: 5,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
    })

</script>
@stop

@section('others')
@parent
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>

<script type="text/javascript" src="js/jquery.simplePagination.js"></script>

<script  type="text/javascript" src="js/jquery.tooltipster.min.js"></script>
<script type="text/javascript" src="js/tabs.js"></script>
<script type="text/javascript" src="js/toptags.js"></script>
<script type="text/javascript" src="js/events.js"></script>
<script type="text/javascript" src="js/tracks.js"></script>
<script src="js/bootstrap.min.js"></script>
<link type="text/css" rel="stylesheet" href="css/tabs.css"/>
<link type="text/css" rel="stylesheet" href="css/toptags.css"/>
<link type="text/css" rel="stylesheet" href="css/simplePagination.css"/>
<link type="text/css" rel="stylesheet" href="css/tooltipster.css"/>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBF-q7zoEgvrlx5T_pb5gSzMIUtx7AhldI&amp;sensor=false"></script>
@stop

@section('content')

<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item active">
            <img src="{{URL::to('../images/Shattered_Vinyl_HD_Wallpaper_by_Sam_Wells.jpg')}}" alt="...">

            <div class="carousel-caption">
                <h3>The Latest Tunes</h3>
                <p>Always up to date! Be the first to get that special new album your friends keep talking about.</p>

            </div>
        </div>
        <div class="item">
            <img src="{{URL::to('../images/header_image.jpg')}}" alt="...">

            <div class="carousel-caption">
                <h3>The Greatest Singles</h3>
                <p>World premiere selection!</p>

            </div>
        </div>
        <div class="item">
            <img src="{{URL::to('../images/Music.jpg')}}" alt="...">

            <div class="carousel-caption">
                <h3>The best Prices</h3>
                <p>You won't find any better prices. But if you do... we'll match it!</p>
                @if(Auth::guest())
                <p><a class="btn btn-lg btn-primary" href="{{URL::to('users/create')}}" role="button">Sign up today</a>
                <a class="btn btn-lg btn-primary" href="{{URL::to('users/login')}}" role="button">Log in</a>
                </p>
                @endif
            </div>
        </div>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
</div>
<!-- Carousel -->


<hr class="featurette-divider">

<div class="container marketing">

    <h3>Best Selling</h3>
    <!-- Three columns of text below the carousel -->
    <div class="row">
        @foreach ($bestselling as $album)
        <div class="col-lg-2">
            <img class="img-circle"
                 src="{{$album->cover_url}}"
                 alt="Generic placeholder image" style="width: 100px; height: 100px;">

            <h4>{{$album->item_name}}</h4>
            <h5>{{$album->author_name}}</h5>

            <p><a class="btn btn-default" href="{{URL::to('store/album/'.$album->item_id)}}" role="button">View details »</a></p>
        </div>
        @endforeach
        <!-- /.col-lg-4 -->
    </div>

    <hr class="featurette-divider">
    <h3>Latest and Greatest</h3>
     <!-- Three columns of text below the carousel -->
        <div class="row">
            @foreach ($latest as $album)
                   <div class="col-lg-2">
                       <img class="img-circle"
                            src="{{$album->cover_url}}"
                            alt="Generic placeholder image" style="width: 100px; height: 100px;">

                       <h4>{{$album->item_name}}</h4>
                       <h5>{{$album->author_name}}</h5>

                       <p><a class="btn btn-default" href="{{URL::to('store/album/'.$album->item_id)}}" role="button">View details »</a></p>
                   </div>
            @endforeach
            <!-- /.col-lg-4 -->

        </div>

     <hr class="featurette-divider">
        <h3>Promotions</h3>
         <!-- Three columns of text below the carousel -->
            <div class="row">
                @foreach ($sales as $album)
                       <div class="col-lg-2">
                           <img class="img-circle"
                                src="{{$album->cover_url}}"
                                alt="Generic placeholder image" style="width: 100px; height: 100px;">

                           <h4>{{$album->item_name}}</h4>
                           <h5>{{$album->author_name}}</h5>
                           <h5>Price:{{$album->price*((100-$album->discount) / 100)}}</h5>
                           <h6>{{$album->discount}} % off</h6>

                           <p><a class="btn btn-default" href="{{URL::to('store/album/'.$album->item_id)}}" role="button">View details »</a></p>
                       </div>
                @endforeach
                <!-- /.col-lg-4 -->

            </div>


    <!-- /.row -->


    <!-- START THE FEATURETTES -->

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-7">
            <h2 class="featurette-heading">Try our new widget, <span
                    class="text-muted">it'll blow your mind!</span></h2>

            <p class="lead">Our widget can help you find your favorite artist top tags and tracks. It can even find out musical events near you! There's no excuse for missing the latest greatest concert. </p>
        </div>
        <div class="col-md-5">


            <div id="tabs" class="tabs" style="width : 450px;">
                <ul>
                    <li><a href="#toptags"><span class="ui-icon ui-icon-star"></span> Top Tags</a></li>
                    <li><a href="#toptracks"><span class="ui-icon ui-icon-heart"></span> Tracks</a></li>
                    <li><a href="#events"><span class="ui-icon ui-icon-key"></span> Events</a></li>
                </ul>
                <!-- Events TAB-->

                <div id="events">
                    <div>
                        <form id="eventsForm" action="javascript:validateFormEvents();" method="get">
                            <table>
                                <tr>
                                    <td><label for="location">Location:</label></td>
                                    <td><input type="text" size="20" id="location"/><span>*</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="max_dist"> Max Distance: </label></td>
                                    <td><input type="text" size="4" id="max_dist"/> Km</td>
                                </tr>
                                <tr>
                                    <td><label for="results"> Results p/ page: </label></td>
                                    <td><input type="text" size="4" id="results"/></td>
                                </tr>
                            </table>
                            <br/>
                            <input type="submit" value="Search" style="position: absolute; right:15%">
                            <br/>
                        </form>
                        <br/>
                    </div>

                    <div id="waitEvents"
                         style="display:none; position: absolute; left: 50%; margin-left: -16px; top:50%"><img
                            src='images/ajax-loader.gif' alt="loading symbol"/></div>

                    <div id="resultsTable" typeof="MusicEvent">
                        <table id="eventsTable" class="bordered" style="visibility:hidden;">
                            <thead>
                            <tr>
                                <th>Event Name</th>
                                <th>Venue</th>
                                <th>Location</th>
                            </tr>
                            </thead>
                        </table>
                        <br/>

                        <div id="pages"></div>
                        <br/>
                        <input type='button' value='Add all Events to Map' onClick='addEventsToMap()'>
                        <input type='button' value='Clear Events' onClick='initialize()' style="float: right">
                        <br/>
                    </div>


                    <div id="googleMap" style="width:410px;height:150px;">
                        <br/>
                    </div>

                </div>

                <div id="toptags" typeof="MusicPlaylist">
                    <div id="waitTags" style="display:none; position: absolute; left: 50%; margin-left: -16px; top:50%">
                        <img
                                src='images/ajax-loader.gif' alt="loading symbol"/></div>
                    <form id="tagsform" action="javascript:getTags();" method="get">
                        <label for="author">Artist:</label>
                        <input type="text" size="15" id="author" property="author"/>
                        <input type="submit" value="Search">
                    </form>
                    <br/>
                    <table id="content" class="bordered" style="visibility:hidden;">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Tag Name</th>
                        </tr>
                        </thead>
                    </table>
                    <br/>

                    <div id="pagination"></div>
                </div>

                <div id="toptracks">
                    <img id="imageW" src="images/warning.ico" alt="warning symbol.">

                    <p id="warning">In order to interact with this tab, you must first select a tag at the previous
                        tab!</p>

                    <div id="waitTracks"
                         style="display:none; position: absolute; left: 50%; margin-left: -16px; top:50%"><img
                            src='images/ajax-loader.gif' alt="loading symbol"/></div>
                    <form id="tracksform" action="javascript:editTracksQuantity();" method="get"
                          style="visibility: hidden;">
                        <label>Quantity</label>
                        <input type="text" size="5" id="tracksQnt" value="4"/>
                        <input type="submit" value="->"/>
                        <input type="hidden" id="tagNameInTracks" value=""/>
                    </form>
                    <div id="trackTable" typeof="MusicRecording">
                    </div>
                </div>
            </div>

        </div>
    </div>

    <hr class="featurette-divider">
</div>



@stop
