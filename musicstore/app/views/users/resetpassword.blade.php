@extends('layouts.master')

@section('title')
@parent
Reset Password
@stop

@section('styles')
@parent
        .maincontent {
            background-color: #FFF;
            margin: auto;
            padding: 20px;
            width: 400px;
            box-shadow: 0 0 20px #AAA;
            position: relative;
            top: 50%;
            transform: translateY(10%);
        }
@stop
@section('content')
<div class="maincontent">
    <h1>Reset Password</h1>
    {{-- Renders the forgot password form of Confide --}}
    {{ Confide::makeResetPasswordForm()->render(); }}
</div>
@stop