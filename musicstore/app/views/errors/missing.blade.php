@extends('layouts.master')

@section('title')
@parent
Music Store Page no found
@stop

@section('content')
<div class="jumbotron">
     <h1>Oops!</h1>
      <p>Looks like you may have taken a wrong turn, this page does not exist.</p>
      <p> Don't worry, it happens to the best of us. </p>
      <p> Here's a little gem, to get you back home safely:</p>
      <p>
      <a class="btn btn-lg btn-primary" href="{{URL::to('/')}}" role="button">Home</a>
      </p>
</div>
@stop