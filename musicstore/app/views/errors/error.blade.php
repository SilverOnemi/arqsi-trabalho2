@extends('layouts.master')

@section('title')
@parent
Music Store Error
@stop

@section('content')
<div class="jumbotron">
     <h1>Oops!</h1>
      <p>Looks like the impossible happened!</p>
      <p>Fear not, a team of highly skilled monkeys has been dispatched to handle the situation. </p>
      <p>If you see them, show them this code: {{$code}}</p>
</div>
@stop