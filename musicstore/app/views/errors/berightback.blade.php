@extends('layouts.master')

@section('title')
@parent
Music Store Error
@stop

@section('content')
<div class="jumbotron">
     <h1>Be right Back!</h1>
     <p>We're humans too! We need breaks every once in a while, specially to bring new and exciting features to you :).</p>
</div>
@stop