<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/test','HomeController@test');

Route::get('/', 'HomeController@showWelcome');
Route::get('store/catalog','StoreController@showCatalog');
Route::post('store/catalog','StoreController@addAlbum');
Route::post('store/catalog/suggestions','StoreController@suggestions');
Route::post('store/catalog/suggestionsString','StoreController@suggestionsString');
Route::get('store/cart','CartController@showCart');
Route::get('store/cart/{id}','CartController@addSingleAlbum');
Route::when('store/cart', 'auth');
Route::get('store/album/{id}','AlbumController@showAlbum');
Route::get('store/clear','CartController@clear');
Route::get('store/success','CartController@receipt');
Route::post('store/clear','CartController@clear');
Route::post('store/checkout/verifystocks','StoreController@checkstocks');
Route::post('store/edit/{id}','CartController@update');
Route::post('store/delete/{id}','CartController@delete');
Route::get('store/checkout','CartController@showCheckout');
Route::when('store/checkout', 'auth');
Route::post('store/checkout','CartController@checkout');



// Dashboard route
Route::get('userpanel/dashboard', 'UserPanelController@showDashboard');
// Applies auth filter to the routes within admin/
Route::when('userpanel/*', 'auth');

// Statistics route
Route::get('statistics/orders/today', 'OrderStatisticsController@getToday');
Route::get('statistics/orders/week', 'OrderStatisticsController@getWeek');
Route::get('statistics/orders/month', 'OrderStatisticsController@getMonth');
Route::get('statistics/orders/years', 'OrderStatisticsController@getYear');
Route::get('statistics/topproducts/today', 'TopProductsStatisticsController@getToday');
Route::get('statistics/topproducts/week', 'TopProductsStatisticsController@getWeek');
Route::get('statistics/topproducts/month', 'TopProductsStatisticsController@getMonth');
Route::get('statistics/topproducts/year', 'TopProductsStatisticsController@getYear');
Route::get('statistics/products/today', 'ProductStatisticsController@getToday');
Route::get('statistics/products/week', 'ProductStatisticsController@getWeek');
Route::get('statistics/products/month', 'ProductStatisticsController@getMonth');
Route::get('statistics/products/year', 'ProductStatisticsController@getYear');
Route::get('statistics/items/stocks', 'ItemStatisticsController@getStock');
Route::get('statistics/items/top/authors', 'ItemStatisticsController@getTopAuthors');
Route::get('statistics/items/top/items', 'ItemStatisticsController@getTopItems');
Route::get('statistics/items/count', 'ItemStatisticsController@getTotalItems');
Route::get('statistics/items/added/year', 'ItemStatisticsController@addedThisYear');
Route::get('statistics/items/added/month', 'ItemStatisticsController@addedThisMonth');
Route::get('statistics/revenue/month', 'RevenueStatisticsController@getMonthlyRevenue');
// Applies auth filter to the routes within admin/
Entrust::routeNeedsRole( 'statistics/*', 'admin' );


// Admin route
Entrust::routeNeedsRole( 'admin/*', 'admin' );
Route::get('admin','AdminController@showDashboard');
Route::get('admin/statistics/orders','AdminController@showStatisticsOrders');
Route::get('admin/statistics/products','AdminController@showStatisticsProducts');
Route::get('admin/statistics/topproducts','AdminController@showStatisticsTopProducts');


Route::get('admin/restock','AdminController@showRestock');
Route::post('admin/restock/additem/{id}','AdminController@addItem');


// Editors route
Route::resource('editors','EditorController');

// Routes
// Only users with roles that have the 'manage_posts' permission will
// be able to access any route within admin/post.
//Entrust::routeNeedsPermission( 'admin/post*', 'manage_posts' );

// Confide routes
Route::get('users/create', 'UsersController@create');
Route::post('users', 'UsersController@store');
Route::get('users/login', 'UsersController@login');
Route::post('users/login', 'UsersController@doLogin');
Route::get('users/confirm/{code}', 'UsersController@confirm');
Route::get('users/forgot_password', 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');
Route::get('users/logout', 'UsersController@logout');
