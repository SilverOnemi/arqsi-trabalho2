<?php

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        $user = new User;
        $user->username = 'admin';
        $user->email = '1120409@isep.ipp.pt';
        $user->password = 'qwerty';
        $user->password_confirmation = 'qwerty';
        $user->confirmation_code = md5(uniqid(mt_rand(), true));

        if (!$user->save()) {
            Log::info('Unable to create user ' . $user->username, (array)$user->errors());
        } else {
            Log::info('Created user "' . $user->username . '" <' . $user->email . '>');
        }

        $user->confirm();

        $user = new User;
        $user->username = 'user';
        $user->email = '1120492@isep.ipp.pt';
        $user->password = 'qwerty';
        $user->password_confirmation = 'qwerty';
        $user->confirmation_code = md5(uniqid(mt_rand(), true));

        if (!$user->save()) {
            Log::info('Unable to create user ' . $user->username, (array)$user->errors());
        } else {
            Log::info('Created user "' . $user->username . '" <' . $user->email . '>');
        }
        $user->confirm();
    }
}