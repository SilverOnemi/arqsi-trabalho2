<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UsersTableSeeder');
		$this->call('UsersRoleSeeder');
		$this->call('AuthorSeeder');
		$this->call('ItemsSeeder');
		$this->call('OrderSeeder');
		$this->call('EditorSeeder');
		$this->call('EditorOrderSeeder');
		$this->call('PromotionSeeder');
	}

}
