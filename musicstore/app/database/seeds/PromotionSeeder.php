<?php
/**
 * Created by IntelliJ IDEA.
 * User: silverone
 * Date: 11/29/14
 * Time: 1:03 AM
 */

class PromotionSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tiesto = Author::whereName('Tiesto')->firstOrFail();
        $paul = Author::whereName('Paul van Dyk')->firstOrFail();

        $promo = new Promotion;
        $promo->discount=20;
        $promo->begin='2014-11-29';
        $promo->end='2014-12-28';
        $promo->save();

        $items=$promo->items();
        $items->attach($tiesto);
        $items->attach($paul);

        $promo->push();
    }
}