<?php
/**
 * Created by IntelliJ IDEA.
 * User: silverone
 * Date: 11/27/14
 * Time: 6:48 PM
 */

class AuthorSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $author = new Author;
        $author->name='Madonna';
        $author->save();

        $author = new Author;
        $author->name='Armin van Buuren';
        $author->save();

        $author = new Author;
        $author->name='Tiesto';
        $author->save();

        $author = new Author;
        $author->name='Paul van Dyk';
        $author->save();

        $author = new Author;
        $author->name='Cold Play';
        $author->save();
    }
}