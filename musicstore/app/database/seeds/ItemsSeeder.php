<?php

class ItemsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $item = new Item;
        $item->author_id=Author::where('name','=','Madonna')->firstOrFail()->id;
        $item->name='Like a virgin';
        $item->genre='pop';
        $item->price=15.2;
        $item->type='Vinyl';
        $item->stock=20;
        $item->cover_url='https://upload.wikimedia.org/wikipedia/en/thumb/1/17/LikeAVirgin1984.png/220px-LikeAVirgin1984.png';
        $item->save();

        $item = new Item;
        $item->author_id=Author::where('name','=','Armin van Buuren')->firstOrFail()->id;
        $item->name='Mirage';
        $item->genre='trance';
        $item->type='Vinyl';
        $item->price=10;
        $item->stock=9;
        $item->cover_url='https://upload.wikimedia.org/wikipedia/en/a/a6/Mirage.jpg';
        $item->save();

        $item = new Item;
        $item->author_id=Author::where('name','=','Tiesto')->firstOrFail()->id;
        $item->name='A Town Called Paradise';
        $item->genre='club';
        $item->type='Vinyl';
        $item->price=5;
        $item->stock=0;
        $item->cover_url='https://upload.wikimedia.org/wikipedia/en/3/38/Tiesto_-_A_Town_Called_Paradise.jpg';
        $item->save();

        $item = new Item;
        $item->author_id=Author::where('name','=','Paul van Dyk')->firstOrFail()->id;
        $item->name='Evolution';
        $item->genre='club';
        $item->type='Vinyl';
        $item->price=18;
        $item->stock=22;
        $item->cover_url='https://upload.wikimedia.org/wikipedia/en/thumb/6/64/EvolutionCover.jpg/220px-EvolutionCover.jpg';
        $item->save();

        $item = new Item;
        $item->author_id=Author::where('name','=','Cold Play')->firstOrFail()->id;
        $item->name='Ghost Stories';
        $item->genre='rock';
        $item->type='Vinyl';
        $item->price=25;
        $item->stock=1;
        $item->cover_url='https://upload.wikimedia.org/wikipedia/en/8/8a/Coldplay_-_Ghost_Stories.png';
        $item->save();
    }
}
