<?php

class EditorOrderSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $order = new EditorOrder;
        $order->user_id=1;
        $order->editor_id=1;

        $item1 =Item::find(1);
        $item2 =Item::find(2);
        $item3 =Item::find(3);
        $item4 =Item::find(4);
        $item5 =Item::find(5);

        $order->quantity=$item1->stock+
                         $item2->stock+
                         $item3->stock+
                         $item4->stock+1+
                         $item4->stock+1;

        $order->total=
              ($item1->price-5)*$item1->stock
            + ($item2->price-5)*$item2->stock
            + ($item3->price-5)*$item3->stock
            + ($item4->price-5)*($item4->stock+1)
            + ($item5->price-5)*($item5->stock+1);

        $order->save();

        $order->items()->attach($item1, array('quantity'=>$item1->stock, 'price'=>$item1->price));
        $order->items()->attach($item2, array('quantity'=>$item2->stock, 'price'=>$item2->price));
        $order->items()->attach($item3, array('quantity'=>$item3->stock, 'price'=>$item3->price));
        $order->items()->attach($item4, array('quantity'=>($item4->stock+1), 'price'=>$item4->price));
        $order->items()->attach($item5, array('quantity'=>($item5->stock+1), 'price'=>$item5->price));

        //saves the order and all its relations
        $order->push();
    }
}
