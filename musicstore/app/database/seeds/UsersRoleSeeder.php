<?php

class UsersRoleSeeder extends Seeder
{

    public function run()
    {
        $admin = new Role;
        $admin->name='admin';
        $admin->save();

        $user = User::where('username','=','admin')->first();
        $user->attachRole($admin);

        $restock = new Permission;
        $restock->name='restock';
        $restock->display_name='Restock';
        $restock->save();
        $admin->attachPermission($restock);
    }
}