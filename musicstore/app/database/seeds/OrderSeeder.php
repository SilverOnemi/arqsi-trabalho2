<?php

class OrderSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $order = new Order;
        $order->user_id=2;
        $order->country='Portugal';
        $order->city='Porto';
        $order->address='Insert address here';
        $order->postal_code='5000-400';
        $order->quantity=2;

        $item1 =Item::find(4);
        $item2 =Item::find(5);


        $order->total=$item1->price+$item2->price;
        $order->save();

        $order->items()->attach(4,array('quantity'=>1, 'price'=>$item1->price));
        $order->items()->attach(5,array('quantity'=>1, 'price'=>$item2->price));

        //saves the order and all its relations
        $order->push();
    }
}
