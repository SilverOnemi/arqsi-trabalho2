<?php
/**
 * Created by IntelliJ IDEA.
 * User: silverone
 * Date: 12/2/14
 * Time: 6:42 PM
 */

class EditorSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $editor = new Editor;
        $editor->name='2DEditor';
        $editor->service_url=null;
        $editor->api_key=null;
        $editor->save();
    }
}