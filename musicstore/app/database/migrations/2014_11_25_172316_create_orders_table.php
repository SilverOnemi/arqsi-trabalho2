<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
			$table->string('country');
			$table->string('city');
			$table->string('address');
			$table->string('postal_code');
			$table->decimal('total',8,2)->default(0);
			$table->smallInteger('quantity')->unsigned();
			$table->timestamps();
		});

		Schema::create('item_order', function(Blueprint $table)
			{
			$table->integer('order_id')->unsigned();
			$table->integer('item_id')->unsigned();
			$table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('item_id')->references('id')->on('items')->onDelete('cascade')->onUpdate('cascade');

			$table->primary(array('order_id','item_id'));
			$table->decimal('price',8,2)->unsigned();
			$table->smallInteger('quantity')->unsigned()->default(1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('item_order');
		Schema::dropIfExists('orders');
	}

}
