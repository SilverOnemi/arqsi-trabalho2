<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('promotions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->tinyInteger('discount')->unsigned();
			$table->date('begin');
			$table->date('end');
			$table->timestamps();
		});

		Schema::create('item_promotion', function(Blueprint $table)
		{
			$table->integer('promotion_id')->unsigned();
			$table->integer('item_id')->unsigned();
			$table->foreign('promotion_id')->references('id')->on('promotions')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('item_id')->references('id')->on('items')->onDelete('cascade')->onUpdate('cascade');
			$table->unique(array('promotion_id','item_id'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('item_promotion');
		Schema::dropIfExists('promotions');
	}

}
