<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEditorOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('editor_orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
			$table->integer('editor_id')->unsigned();
			$table->foreign('editor_id')->references('id')->on('editors')->onDelete('cascade')->onUpdate('cascade');
			$table->decimal('total',8,2)->unsigned();
			$table->smallInteger('quantity')->unsigned();
			$table->timestamps();
		});

		Schema::create('editor_order_item', function(Blueprint $table)
		{
			$table->integer('editor_order_id')->unsigned();
			$table->integer('item_id')->unsigned();
			$table->foreign('editor_order_id')->references('id')->on('editor_orders')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('item_id')->references('id')->on('items')->onDelete('cascade')->onUpdate('cascade');

			$table->primary(array('editor_order_id','item_id'));
			$table->decimal('price',8,2)->unsigned();
			$table->smallInteger('quantity')->unsigned()->default(1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('editor_order_item');
		Schema::dropIfExists('editor_orders');
	}

}
