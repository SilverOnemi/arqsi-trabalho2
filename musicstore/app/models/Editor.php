<?php
/**
 * Created by IntelliJ IDEA.
 * User: silverone
 * Date: 12/2/14
 * Time: 6:44 PM
 */

class Editor extends Eloquent{
    protected $fillable = array('name','service_url','api_key');

    public function editorOrders(){
        return $this->hasMany('EditorOrder');
    }
}