<?php

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
 /* The Role model has two main attributes: name and permissions.
    name, as you can imagine, is the name of the Role. For example: "Admin", "Owner", "Employee".
    The permissions field has been deprecated in preference for the permission table.
    You should no longer use it. It is an array that is automatically serialized and unserialized
    when the Model is saved. This array should contain the name of the permissions of the Role.
    For example: array( "manage_posts", "manage_users", "manage_products" ).
 */
}