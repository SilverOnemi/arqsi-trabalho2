<?php
/**
 * Created by IntelliJ IDEA.
 * User: silverone
 * Date: 11/25/14
 * Time: 4:50 PM
 */

class Item extends Eloquent{
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];

    protected $fillable= array('author','name',
        'genre','price','stock','cover_url');

    public function author(){
        return $this->belongsTo('Author');
    }

    public function items(){
        return $this->belongsToMany('Promotion');
    }
}