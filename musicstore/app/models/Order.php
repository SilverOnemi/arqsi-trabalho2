<?php
/**
 * Created by IntelliJ IDEA.
 * User: silverone
 * Date: 11/25/14
 * Time: 4:49 PM
 */

class Order extends Eloquent {

    protected $fillable = array('user_id','country',
        'city','address','postal_code','total');

    public function items(){
        return $this->belongsToMany('Item')->withPivot('quantity','price');
    }

    public function user(){
        return $this->belongsTo('User');
    }
}