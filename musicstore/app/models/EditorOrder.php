<?php
/**
 * Created by IntelliJ IDEA.
 * User: silverone
 * Date: 11/27/14
 * Time: 2:58 PM
 */

class EditorOrder extends Eloquent{
    protected $table = 'editor_orders';

    protected $fillable = array('user_id','country',
        'city','address','postal_code','price','editor_id');

    public function items(){
        return $this->belongsToMany('Item');
    }

    public function editor(){
        $this->belongsTo('Editor');
    }
}