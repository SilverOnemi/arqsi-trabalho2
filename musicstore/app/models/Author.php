<?php

class Author extends Eloquent {
    public $timestamps = false;

    protected $fillable = array('name');

    public function items(){
        return $this->hasMany('Item');
    }
}