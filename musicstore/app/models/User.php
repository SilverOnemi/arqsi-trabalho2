<?php 
use Zizaco\Confide\ConfideUser; 
use Zizaco\Confide\ConfideUserInterface;
use Zizaco\Entrust\HasRole;

 
class User extends Eloquent implements ConfideUserInterface { 
    use ConfideUser;

    /*
     * This will do the trick to enable the relation with Role and the following methods roles,
     * hasRole( $name ), can( $permission ), and ability($roles, $permissions, $options)
     * within your User model.
    */
    use HasRole;

    public function cart(){
        return $this->hasOne('Cart');
    }

}
