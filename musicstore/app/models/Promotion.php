<?php
/**
 * Created by IntelliJ IDEA.
 * User: silverone
 * Date: 11/29/14
 * Time: 1:05 AM
 */

class Promotion extends Eloquent {
    protected $fillable = array('percentage','begin',
                                'end');

    public function items(){
        return $this->belongsToMany('Item');
    }
}