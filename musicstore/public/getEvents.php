<?php
include("DAL.php");
//atribui às variáveis os dados recebidos pelo get
$location = $_GET["location"];
$distance = $_GET["distance"];
$results = $_GET["results"];

queryLastFM();


/**
 * Queries LastFM for the events on a specific location within a specific range,
 * processes the reply,
 * and echoes the requested data in JSON. The function asks for JSON
 * formatted data and with auto correct enabled.
 */
function queryLastFM()
{
    global $location;
    global $distance;
    $location = urlencode($location);


    if (isset($location) && isset($distance)) {
        $lastFMQuery = "http://ws.audioscrobbler.com/2.0/?method=geo.getevents&location=" . $location . "&distance=" . $distance . "&limit=100&api_key=9e5978ffe340045b07f2420dc925e3ee&format=json";

        processLastFMReply(file_get_contents($lastFMQuery));

    } else if (isset($location)) {
        $lastFMQuery = "http://ws.audioscrobbler.com/2.0/?method=geo.getevents&location=" . $location . "&limit=100&api_key=9e5978ffe340045b07f2420dc925e3ee&format=json";


        processLastFMReply(file_get_contents($lastFMQuery));
    } else {
        echo '{"error": -1, "message":"No location get parameter.","links":[]}';
    }
}

/**
 * Decode the lastFMReply JSON, check and log errors,
 * then process the reply.
 * @param $lastFMReply String The JSON formatted LastFM Reply.
 */
function processLastFMReply($lastFMReply)
{
    if ($lastFMReply) {
        global $location;
        global $distance;
        global $results;
        $dal = new DAL();
        $location = urldecode($location);

        if (!empty($_SERVER['HTTP_CLIENT_IP']))
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else
            $ip = $_SERVER['REMOTE_ADDR'];


        if (isset($results) && isset($distance))
            $dal->db_insertEventSearch($ip, $location, $distance, $results);

        else if (isset($distance))
            $dal->db_insertEventSearch($ip, $location, $distance, "5");

        else if (isset($results))
            $dal->db_insertEventSearch($ip, $location, "20", $distance);

        else
            $dal->db_insertEventSearch($ip, $location, "20", "5");


        echo $lastFMReply;
    } else {
        echo '{"error": -2, "message":"Could not query LastFM.","links":[]}';
    }
}


//JSON TUTORIAL
/*webtutsdepot.com/2009/08/31/how-to-read-json-data-with-php/*/
?>


