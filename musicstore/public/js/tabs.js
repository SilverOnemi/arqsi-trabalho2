var tabRecized = 0;

$(function () {
    $("#tabs").tabs({
        collapsible: true,
        beforeLoad: function (event, ui) {
            ui.jqXHR.error(function () {
                ui.panel.html("Couldn't load this tab. We'll try to fix this as soon as possible. ");
            });
        },

        activate: function (event, ui) {
            if (ui.newPanel.selector == "#events" && tabRecized == 0) {
                tabRecized = 1;
                updateMap();
            }
        }
    });

});
