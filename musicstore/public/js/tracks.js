/**
 * Creates a XLMHTTPRequestObject.
 * @returns {*} the xmlHttpObj
 */

function createTracksXmlHttpRequestObject() {
    var xmlHttpObj;

    if (window.XMLHttpRequest)
        xmlHttpObj = new XMLHttpRequest();
    else if (window.ActiveXObject)
        xmlHttpObj = new ActiveXObject("Microsoft.XMLHTTP");

    return xmlHttpObj;
}
/**
 * Calls createTracksXmlHttpRequestObject and processes the request.
 * @param $tag the tag name
 * @param $quantity quantity of tracks
 */
function updateTracks(tag, quantity) {
    document.getElementById("tagNameInTracks").value = tag;

    xmlHttpObj = createTracksXmlHttpRequestObject();
    if (xmlHttpObj) {
        var link = "getTracks.php?tag=" + tag + "&quantity=" + quantity;
        xmlHttpObj.onreadystatechange = tracksStateHandler;
        xmlHttpObj.open("GET", link, true);
        xmlHttpObj.send();
    	$("#waitTracks").css("display", "block");
    } else
        alert("Could not create xml http request object");
}

/**
 * StateHandler for the top tags refresh.
 *
 * Receives data in XML and parses the data, checks it
 * for errors, checks if its empty and finally updates
 * the tag table and the author.s
 */
function tracksStateHandler() {
    var tracks = xmlHttpObj.responseText;
    if (xmlHttpObj.readyState == 4 && xmlHttpObj.status == 200) {
        if (!checkForTracksErrors(tracks)) {
            document.getElementById("trackTable").innerHTML = tracks;
            document.getElementById("tracksform").style.visibility = "visible";
            /** jQuery code that consumes the title of tags that have the 'tooltip' class and adds a tooltip */
            $('.tooltipp').tooltipster({position: 'right', contentAsHTML: true});
            $("#waitTracks").css("display", "none");
        } else {
            alert("An error occurred");
        }
    }
}

/**
 * Verifies if it is an integer
 *
 * @param value
 * @returns {boolean}
 */
function isInt(value) {
    return !isNaN(value) &&
        parseInt(Number(value)) == value && !isNaN(parseInt(value, 10));
}

/**
 * checks if we did not receive an error message.
 * @param tracks the data
 * @returns {number} one if an error occurred, zero if it did not.
 */
function checkForTracksErrors(tracks) {
    if (tracks.error) {
        alert(tracks["message"]);
        return 1;
    }

    return 0;
}

/**
 * Checks if the list is empty and updates the author input.
 * @param tracks
 * * @returns {number} one if an error occurred, zero if it did not.
 */
function checkEmptyTracksList(tracks) {
    if (tracks["toptracks"]["track"]) {
        var author = document.getElementById("author");
        author.value = "artist not found.";

        return 1;
    }

    return 0;
}

/**
 * Function that changes the quantity of rows
 */
function editTracksQuantity() {
    var tagsForm = document.forms["tracksform"];
    var tag = tagsForm["tagNameInTracks"].value;
    var quantity = tagsForm["tracksQnt"].value;
    if (isInt(quantity))
        updateTracks(tag, quantity);
    else
        alert("Quantity must be a integer");
}
