
/**
 * Control flux variable
 */

var flag = 0;

/**
 * Creates a XLMHTTPRequestObject.
 * @returns {*} the xmlHttpObj
 */

function createTagsXmlHttpRequestObject() {
    var xmlHttpObj;

    if (window.XMLHttpRequest)
        xmlHttpObj = new XMLHttpRequest();
    else if (window.ActiveXObject)
        xmlHttpObj = new ActiveXObject("Microsoft.XMLHTTP");

    return xmlHttpObj;
}

/**
 * Asks the server for the N top tags of the author.
 */
function getTags() {
    var tagsForm = document.forms["tagsform"];
    var author = tagsForm["author"].value;

    if (!tagsValidateForm(author)) {
        xmlHttpObj = createTagsXmlHttpRequestObject();
        if (xmlHttpObj) {
            var link = "getTopTags.php?author=" + author;
            xmlHttpObj.onreadystatechange = tagsStateHandler;
            xmlHttpObj.open("GET", link, true);
            xmlHttpObj.send();
            $("#waitTags").css("display", "block");
        } else
            alert("Could not create xml http request object");
    }
}

/**
 * StateHandler for the top tags refresh.
 *
 * Receives data in JSON and parses the data, checks it
 * for errors, checks if its empty and finally updates
 * the tag table and the author.s
 */
function tagsStateHandler() {
    if (xmlHttpObj.readyState == 4 && xmlHttpObj.status == 200) {
        var tags = JSON.parse(xmlHttpObj.responseText);

        if (!checkForErrors(tags) && !checkEmptyList(tags)) {
            updateTagsTable(tags);
            updateAuthor(tags);
            $("#waitTags").css("display", "none");
        }
    }
}

/**
 * Updates the tags table.
 * @param tags the tag table objects.
 */
function updateTagsTable(tags) {
    var tagsTable = document.getElementById("content");
    tagsTable.deleteTFoot();
    var footer = tagsTable.createTFoot();

    var tagsArray = tags["toptags"]["tag"];
    for (var tag in tagsArray) {
        var row = footer.insertRow(-1);
        var currentTag = tagsArray[tag];

        var cell = row.insertCell(-1);
        cell.innerHTML = currentTag.count;


        cell = row.insertCell(-1);

        var a = document.createElement("a");
        a.href = "javascript:switchTabs(\"" + currentTag.name + "\");";
        a.innerHTML = currentTag.name;
        a.setAttribute("property", "genre");
        cell.appendChild(a);
    }

    tagsTable.style.visibility = "visible";

    jQuery(function ($) {
        var items = $("#content tfoot tr");

        var numItems = items.length;
        var perPage = 5;

        // only show the first 2 (or "first per_page") items initially
        items.slice(perPage).hide();

        // now setup pagination
        $("#pagination").pagination({
            items: numItems,
            itemsOnPage: perPage,
            cssStyle: "compact-theme",
            onPageClick: function func(pageNumber) { // this is where the magic happens
                // someone changed page, lets hide/show trs appropriately
                var showFrom = perPage * (pageNumber - 1);
                var showTo = showFrom + perPage;

                items.hide() // first hide everything, then show for the new page
                    .slice(showFrom, showTo).show();
            },
            displayedPages: 3
        });
    });
}


/**
 * Switches to tab nr 2.
 */
function switchTabs(tag) {
    $("#tabs").tabs("option", "active", 1);
    if(flag===0){
        var toptracks = document.getElementById("toptracks");
        var img = document.getElementById("imageW");
        var warning = document.getElementById("warning");
        toptracks.removeChild(img);
        toptracks.removeChild(warning);
        flag = 1;
    }

    updateTracks(tag, 4);
}

/**
 * Updates the author
 * @param tags the data.
 */
function updateAuthor(tags) {
    var author = document.getElementById("author");
    author.value = tags["toptags"]["@attr"]["artist"];
}

/**
 * checks if we did not receive an error message.
 * @param tags the data
 * @returns {number} one if an error occurred, zero if it did not.
 */
function checkForErrors(tags) {
    if (tags.error) {
        alert(tags["message"]);
        return 1;
    }

    return 0;
}

/**
 * Checks if the list is empty and updates the author input.
 * @param tags
 * * @returns {number} one if an error occurred, zero if it did not.
 */
function checkEmptyList(tags) {
    if (tags["toptags"]["artist"]) {
        var author = document.getElementById("author");
        author.value = "artist not found.";

        return 1;
    }

    return 0;
}

/**
 * Validate the input form
 * @param artist the artists name
 * @param numberOfTags the number of tags
 * @returns {number} 0 if the form is valid, 1 if it is not.
 */
function tagsValidateForm(artist) {
    if (artist.length == 0) {
        var author = document.getElementById("author");
        author.value = "Author cannot be empty!";
    }
    return 0;
}





