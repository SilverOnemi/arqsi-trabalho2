/*Google Maps function*/
var map;
var mapProp;
var message;
function initialize() {

    mapProp = {
        center: new google.maps.LatLng(41.177980, -8.608262),
        zoom: 5,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
}

/*Google Maps function*/


//Events javascript code
var events_json;
/**
 *
 *Validate if a location is set, and then, calls getEvents().
 *
 */
function validateFormEvents() {
    var eventForm = document.forms["eventsForm"];
    var location = eventForm["location"].value;
    if (location == null || location == "") {
        alert("You need to specify a location!");
    }
    else {
        $("#waitEvents").css("display", "block");
        getEvents();
    }
}

/**
 * Creates a XLMHTTPRequestObject.
 * @returns {*} the xmlHttpObj
 */
function createEventsHttpRequestObject() {
    var httpobj;
    if (window.XMLHttpRequest) {
        httpobj = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        httpobj = new ActiveXObject("Microsoft.XMLHTTP");
    }
    return httpobj;
}

/**
 * Asks the server for the events on a specific location, and if possible
 * within a specific distance.
 */
function getEvents() {
    var eventForm = document.forms["eventsForm"];
    var location = eventForm["location"].value;
    var m_distance = eventForm["max_dist"].value;
    var results = eventForm["results"].value;
    httpobj = createEventsHttpRequestObject();
    if (m_distance == null || m_distance == "") {
        if (httpobj) {
            var link = "getEvents.php?location=" + location + "&results=" + results;
            httpobj.onreadystatechange = stateHandlerEvents;
            httpobj.open("GET", link, true);
            httpobj.send();
        } else
            alert("Could not create http request object");
    }
    else {
        if (httpobj) {
            var link = "getEvents.php?location=" + location + "&distance=" + m_distance + "&results=" + results;
            httpobj.onreadystatechange = stateHandlerEvents;
            httpobj.open("GET", link, true);
            httpobj.send();
        } else
            alert("Could not create http request object");
    }
}

/**
 * StateHandler for the top tags refresh.
 *
 * Receives data in JSON and parses the data, checks it
 * for errors, checks if its empty and finally updates
 * the tag table and the author.s
 */
function stateHandlerEvents() {
    if (httpobj.readyState == 4 && httpobj.status == 200) {
        events_json = JSON.parse(httpobj.responseText);
        if (!checkForErrors(events_json)) {
            updateEventsTable(events_json);
        }

        $("#waitEvents").css("display", "none");
    }
}

/**
 * checks if we did not receive an error message.
 * @param events the data
 * @returns {number} one if an error occurred, zero if it did not.
 */
function checkForErrors(events) {
    if (events.error) {
        alert(events["message"]);
        return 1;
    }

    return 0;
}

function updateEventsTable(events) {
    var eventForm = document.forms["eventsForm"];
    var no_results = parseInt(eventForm["results"].value);
    if (isNaN(no_results)) {
        no_results = 5;
    }
    var table = document.getElementById("eventsTable");
    table.deleteTFoot();
    var footer = table.createTFoot();
    var eventsArray = events["events"]["event"];
    for (event in eventsArray) {
        var row = footer.insertRow(-1);
        var currentEvent = eventsArray[event];
        var cell = row.insertCell(-1);
        cell.innerHTML = currentEvent.title;
        cell.setAttribute("property", "name");

        cell = row.insertCell(-1);
        cell.innerHTML = currentEvent.venue.name + ", " + currentEvent.startDate;
        cell.setAttribute("property", "venue");

        cell = row.insertCell(-1);
        cell.innerHTML = currentEvent.venue.location.city + ", " + currentEvent.venue.location.country;
        cell.setAttribute("property", "location");


    }

    table.style.visibility = "visible";
    jQuery(function ($) {
        var items = $("#eventsTable tfoot tr");

        var numItems = items.length;
        var perPage = no_results;

        // only show the first 2 (or "first per_page") items initially
        items.slice(perPage).hide();

        // now setup pagination
        $("#pages").pagination({
            items: numItems,
            itemsOnPage: perPage,
            cssStyle: "compact-theme",
            onPageClick: function func(pageNumber) { // this is where the magic happens
                // someone changed page, lets hide/show trs appropriately
                var showFrom = perPage * (pageNumber - 1);
                var showTo = showFrom + perPage;

                items.hide() // first hide everything, then show for the new page
                    .slice(showFrom, showTo).show();
            },
            displayedPages: 3
        });
    });

}


function addEventsToMap() {
    var eventsArray = events_json["events"]["event"];
    for (event in eventsArray) {
        var currentEvent = eventsArray[event];
        var lat = parseFloat(currentEvent.venue.location["geo:point"]["geo:lat"]);
        var long = parseFloat(currentEvent.venue.location["geo:point"]["geo:long"]);
        var title = currentEvent.title;
        placeMarkers(lat, long, title);

    }

}

function placeMarkers(lat, long, title) {
    var myLatlng = new google.maps.LatLng(lat, long);
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: title
    });

    var infowindow = new google.maps.InfoWindow({

        content: title
    });

    google.maps.event.addListener(marker, 'click', function () {
        infowindow.open(map, marker);
    });


    google.maps.event.addListener(marker, 'click', function () {
        map.setCenter(marker.getPosition());
    });
    map.setCenter(myLatlng);

}

function updateMap() {

    google.maps.event.trigger(map, 'resize');
    map.setZoom(map.getZoom());

}

