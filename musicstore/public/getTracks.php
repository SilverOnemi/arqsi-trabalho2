<?php
include("DAL.php");
queryLastFM();
$errors = '';
/**
 * Queries LastFM for the tags top tracks, processes the reply, and
 * echoes the requested data in XML. The function asks for XML formatted data
 */
function queryLastFM(){
    $tag = $_GET["tag"];
    $limit = $_GET["quantity"];
    global $errors;

    if (isset($tag)) {
        if(!isset($limit))
            $limit=5;
        $lastFMQuery = "http://ws.audioscrobbler.com/2.0/?method=tag.gettoptracks&tag="
            . urlencode($tag) . "&limit=". urlencode($limit) . "&api_key=9e5978ffe340045b07f2420dc925e3ee";

        processTracksFMReply(file_get_contents($lastFMQuery));
    } else
        $errors .=  'Error 0: No tag <br/>';
}

/**
 * Decode the lastFMReply XML, check and log errors,
 * then process the reply.
 * @param $lastFMReply String The XML formatted LastFM Reply.
 */
function processTracksFMReply($lastFMReply){
    global $errors;
    
    $dal = new DAL();
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else
        $ip = $_SERVER['REMOTE_ADDR'];
    
    if ($lastFMReply){
        $xmlDoc = new DOMDocument('1.0', 'ISO-8859-1');
        $xmlDoc->loadXML($lastFMReply);

        $nodelist=$xmlDoc->getElementsByTagName("track");
        echo "<table id=\"tracks\" class=\"bordered\"> <thead> <th>Track Name</th> </thead>";

        for ($i=0;$i<$nodelist->length;$i++) {
            /** $tagNode is the name of the song
             *  $mbdiTrack is the id of the track */
                $tagNode=$nodelist->item($i)->getElementsByTagName('name')->item(0)->childNodes->item(0)->nodeValue;
                $mbdiTrack=$nodelist->item($i)->getElementsByTagName('mbid')->item(0)->childNodes->item(0)->nodeValue;
                $artist=$nodelist->item($i)->getElementsByTagName("artist")->item(0)->getElementsByTagName('name')->item(0)->childNodes->item(0)->nodeValue;

            /** Block of code that tries to get the artist name, album name and artist/album mbid for future queries */
                //$trackInfoQuery = "http://ws.audioscrobbler.com/2.0/?method=track.getinfo&mbid=". urlencode($mbdiTrack) ."&api_key=9e5978ffe340045b07f2420dc925e3ee";
            $trackInfoQuery = "http://ws.audioscrobbler.com/2.0/?method=track.getinfo&artist=". urlencode($artist) ."&track=".urlencode($tagNode)."&api_key=9e5978ffe340045b07f2420dc925e3ee";
                $trackInfoQueryContent = file_get_contents($trackInfoQuery);
                    if ($trackInfoQueryContent){
                        $xmlDocTrackInfo = new DOMDocument('1.0', 'ISO-8859-1');
                        $xmlDocTrackInfo->loadXML($trackInfoQueryContent);
                        //$artist=$xmlDocTrackInfo->getElementsByTagName("track")->item(0)->getElementsByTagName('artist')->item(0)->getElementsByTagName('name')->item(0)->childNodes->item(0)->nodeValue;
                        $mbidArtist=$xmlDocTrackInfo->getElementsByTagName("track")->item(0)->getElementsByTagName('artist')->item(0)->getElementsByTagName('mbid')->item(0)->childNodes->item(0)->nodeValue;
                        $albumName=$xmlDocTrackInfo->getElementsByTagName("album")->item(0)->getElementsByTagName('title')->item(0)->childNodes->item(0)->nodeValue;
                        $mbdiAlbum=$xmlDocTrackInfo->getElementsByTagName("album")->item(0)->getElementsByTagName('mbid')->item(0)->childNodes->item(0)->nodeValue;

                    /** Block of code that tries to get the artist image */
                        $artistInfoQuery = "http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&mbid="
                            . urlencode($mbidArtist) ."&api_key=9e5978ffe340045b07f2420dc925e3ee";
                        $artistInfoQueryContent = file_get_contents($artistInfoQuery);
                        if ($artistInfoQueryContent){
                            $xmlDocArtistInfo = new DOMDocument('1.0', 'ISO-8859-1');
                            $xmlDocArtistInfo->loadXML($artistInfoQueryContent);
                            $artistNode=$xmlDocArtistInfo->getElementsByTagName("artist")->item(0);
                            $imageArtist=($artistNode->getElementsByTagName('image')->item(2)->childNodes->item(0)->nodeValue);// == 'large'){

                        }else
                            $errors .=   'Error 2: Invalid mbid: '.$mbidArtist. " in the song ".$tagNode."<br/>";

                    /** Block of code that tries to get the top albums */
                        $topAlbumsInfoQuery = "http://ws.audioscrobbler.com/2.0/?method=artist.gettopalbums&mbid="
                            . urlencode($mbidArtist) ."&limit=3&api_key=9e5978ffe340045b07f2420dc925e3ee";
                        $topAlbumsInfoQueryContent = file_get_contents($topAlbumsInfoQuery);
                        if ($topAlbumsInfoQueryContent){
                            $xmlDocTopAlbumsInfo = new DOMDocument('1.0', 'ISO-8859-1');
                            $xmlDocTopAlbumsInfo->loadXML($topAlbumsInfoQueryContent);
                            $nodeAlbumslist=$xmlDocTopAlbumsInfo->getElementsByTagName("album");
                            for ($j=0;$j<$nodeAlbumslist->length;$j++) {
                                $topAlbums[$j]= $nodeAlbumslist->item($j)->getElementsByTagName('name')->item(0)->childNodes->item(0)->nodeValue;
                            }
                        }else
                            $errors .=  'Error 3: Invalid mbid: '.$mbidArtist. " in the song ".$tagNode."<br/>";

                    /** Block of code that tries to get the top tracks */
                        $topTrackInfoQuery = "http://ws.audioscrobbler.com/2.0/?method=artist.gettoptracks&mbid="
                            . urlencode($mbidArtist) ."&limit=1&api_key=9e5978ffe340045b07f2420dc925e3ee";
                        $topTrackInfoQueryContent = file_get_contents($topTrackInfoQuery);
                        if ($topTrackInfoQueryContent){
                            $xmlDocTopTrackInfo = new DOMDocument('1.0', 'ISO-8859-1');
                            $xmlDocTopTrackInfo->loadXML($topTrackInfoQueryContent);
                            $topTrackName=$xmlDocTopTrackInfo->getElementsByTagName("track")->item(0)->getElementsByTagName('name')->item(0)->childNodes->item(0)->nodeValue;
                        }else
                            $errors .=  'Error 4: Invalid mbid: '.$mbidArtist. " in the song ".$tagNode."<br/>";

                    /** Block of code that tries to get the album cover */
                        $albumCoverQuery = "http://coverartarchive.org/release/". urlencode($mbdiAlbum);
                        $albumCoverInfoQueryContent = file_get_contents($albumCoverQuery);
                        if ($albumCoverInfoQueryContent){

                            $jsonAlbumCoverContent = json_decode( $albumCoverInfoQueryContent, true );
                            $imageAlbum = $jsonAlbumCoverContent['images'][0]['thumbnails'][large];

                        }else
                        {$errors .= "Error 6: Invalid mbid: ". $mbdiTrack. " in the song ".$tagNode."<br/>";$imageAlbum="";
                            /** Block of code that tries to get the top tracks */
                            $AlbumInfoQuery = "http://ws.audioscrobbler.com/2.0/?method=album.getinfo&album="
                                . urlencode($albumName) ."&artist=".urlencode($artist)."&api_key=9e5978ffe340045b07f2420dc925e3ee";
                            $AlbumInfoQueryContent = file_get_contents($AlbumInfoQuery);
                            if ($AlbumInfoQueryContent){
                                $xmlDocAlbumInfo = new DOMDocument('1.0', 'ISO-8859-1');
                                $xmlDocAlbumInfo->loadXML($AlbumInfoQueryContent);
                                $imageAlbum=$xmlDocAlbumInfo->getElementsByTagName("album")->item(0)->getElementsByTagName('image')->item(3)->childNodes->item(0)->nodeValue;
                            }else
                                $errors .=  'Error 7: Invalid album : '.$albumName. " and/or artist :".$artist." in the song ".$tagNode."<br/>";
                        }

                    /** The Tooltip variable $title. */
                    $title="&lt;table  class=&quot;bordered2&quot;&gt;
                                 &lt;tr&gt;
                                   &lt;td class=&quot;colored&quot; rowspan=&quot;2&quot;&gt;Artist&lt;br&gt;&lt;/td&gt;
                                   &lt;td rowspan=&quot;2&quot; property=&quot;author&quot; &gt;".$artist."&lt;/td&gt;
                                   &lt;td colspan=&quot;2&quot; rowspan=&quot;2&quot;&gt;&lt;img property=&quot;image&quot; src=&quot;".$imageArtist."&quot;/&gt;&lt;/td&gt;
                                 &lt;/tr&gt;
                                 &lt;tr&gt;
                                 &lt;/tr&gt;
                                 &lt;tr&gt;
                                    &lt;td class=&quot;colored&quot;&gt;Top Albums&lt;/td&gt;
                                    &lt;td &gt;".$topAlbums[0]."&lt;/td&gt;
                                    &lt;td &gt;".$topAlbums[1]."&lt;/td&gt;
                                    &lt;td &gt;".$topAlbums[2]."&lt;/td&gt;
                                 &lt;/tr&gt;
                                 &lt;tr&gt;
                                    &lt;td class=&quot;colored&quot;&gt;Album&lt;/td&gt;
                                    &lt;td property=&quot;inAlbum&quot; &gt;".$albumName."&lt;/td&gt;
                                    &lt;td colspan=&quot;2&quot; rowspan=&quot;2&quot;&gt;"."&lt;img style=&quot;max-width: 120px;&quot; src=&quot;".$imageAlbum."&quot;/&gt;"."&lt;/td&gt;
                                 &lt;/tr&gt;
                                 &lt;tr&gt;
                                    &lt;td class=&quot;colored&quot;&gt;Top Track&lt;/td&gt;
                                    &lt;td &gt;".$topTrackName."&lt;/td&gt;
                                 &lt;/tr&gt;
                            &lt;/table&gt;";
                        
                    $top3Albums = $topAlbums[0].", ".$topAlbums[2].", ".$topAlbums[3];
                        $dal->db_insertTrackSearch($ip,$tagNode,$mbdiTrack,$albumName,$top3Albums,$topTrackName,$artist);

                    /** */
                    echo "<tr><td class=\"tooltipp\" title=\"".$title."\" property=\"name\">".$tagNode . "</td></tr>";

                }else
                    $errors .= "Error 5: Invalid mbid: ". $mbdiTrack. " in the song ".$tagNode."<br/>";
                    $dal->db_insertErrors($ip,$errors);


        }
        echo "</table></br></br></br></br>";

        /**foreach ($nodelist->childNodes AS $item) {
            print $item->nodeName . " = " . $item->nodeValue . "<br>";
        }*/
       }
    else
        $errors = errors . 'Error 1: Invalid tag/limit<br/>';
        $dal->db_insertErrors($ip,$errors);
    
        //echo "<span style=\"font-size:15px;\">".$errors."</span>";
}
?>
