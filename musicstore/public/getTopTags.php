<?php
include("DAL.php");
queryLastFM();

/**
 * Queries LastFM for the authors top number tags, processes the reply,
 * and echoes the requested data in JSON. The function asks for JSON
 * formatted data and with auto correct enabled.
 */
function queryLastFM()
{
    $author = $_GET["author"];


    if (isset($author)) {
        $lastFMQuery = "http://ws.audioscrobbler.com/2.0/?method=artist.gettoptags&artist="
            . urlencode($author) . "&api_key=9e5978ffe340045b07f2420dc925e3ee&format=json&autocorrect=1";

        $dal = new DAL();
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else
            $ip = $_SERVER['REMOTE_ADDR'];

        $dal->db_insertTagSearch($ip, $author);
        processLastFMReply(file_get_contents($lastFMQuery));
    } else
        echo '{"error": -1, "message":"No author get parameter.","links":[]}';
}

/**
 * Decode the lastFMReply JSON, check and log errors,
 * then process the reply.
 * @param $lastFMReply String The JSON formatted LastFM Reply.
 */
function processLastFMReply($lastFMReply)
{
    if ($lastFMReply)
        echo $lastFMReply;
    else
        echo '{"error": -2, "message":"Could not query LastFM.","links":[]}';
}

?>
