<?php

class DAL
{
    private $DB_NAME = 'arqsi';
    private $DB_USER = 'mySQLLocalHost';
    private $DB_HOST = 'localhost';
    private $DB_PASS = 'dVqyQPuC8L8K2LTD';

    /* Function to connect to database
     *
     * @params void
     *
     */

    function db_insertEventSearch($ip, $loc, $dist, $results)
    {
        $mysqli = $this->db_connect();
        if ($mysqli) {
            $sqlquery = "INSERT INTO `events` (`ip`,`location`,`maxDistance`,`resultsPerPage`)  VALUES ('" . $mysqli->real_escape_string($ip) . "','" . $mysqli->real_escape_string($loc) . "','" . $mysqli->real_escape_string($dist) . "','" . $mysqli->real_escape_string($results) . "')";
            $result = $mysqli->query($sqlquery);
        }
    }

    /* Function to save at database a Event search and its params
     *
     * @params $ip - User ip address, $loc - location to search for events, $dist - search radius
     * $results - number of results per page
     *
     */

    function db_insertTagSearch($ip, $tag)
    {
        $mysqli = $this->db_connect();
        if ($mysqli) {
            $sqlquery = "INSERT INTO `toptags` (`ip`,`tag`)  VALUES ('" . $mysqli->real_escape_string($ip) . "','" . $mysqli->real_escape_string($tag) . "')";
            $result = $mysqli->query($sqlquery);
        }

    }


    /* Function to save at database a topTag search and its params
     *
     * @params $ip - User ip address, $artist - artist  to search for TopTags
     *
     */

    function db_insertTrackSearch($ip, $trackName, $trackID, $albumName, $top3Albums, $topTrackName, $artist)
    {
        $mysqli = $this->db_connect();
        if ($mysqli) {
            $sqlquery = "INSERT INTO `tracks`(`ip`, `trackName`, `trackID`, `albumName`, `top3Albums`, `topTrackName`, `artist`) VALUES ('" . $mysqli->real_escape_string($ip) . "','" . $mysqli->real_escape_string($trackName) . "','" . $mysqli->real_escape_string($trackID) . "','" . $mysqli->real_escape_string($albumName) . "','" . $mysqli->real_escape_string($top3Albums) . "','" . $mysqli->real_escape_string($topTrackName) . "','" . $mysqli->real_escape_string($artist) . "')";


            $result = $mysqli->query($sqlquery);
        }

    }

    /* Function to save at database a topTrack search and its params
     *
     * @params $ip - User ip address, $trackName - trackName to perform
     * search, $trackID - track mbid, $albumName - album name that contains the track,
     * $top3Albums - Top 3 albums of artist,
     * $topTrackName - Top track of the album
     *
     */

    function db_insertErrors($ip, $error)
    {
        $mysqli = $this->db_connect();
        $mysqli = $this->db_connect();
        if ($mysqli) {
            $sqlquery = "INSERT INTO `errors`(`ip`, `description`) VALUES ('" . $mysqli->real_escape_string($ip) . "','" . $mysqli->real_escape_string($error) . "')";
            $result = $mysqli->query($sqlquery);
        }
    }

    /* Function to save at database a topTag search and its params
     *
     * @params $ip - User ip address, $error - error gived
     *
     */

    private function db_connect()
    {
        $mysqli = new mysqli($this->DB_HOST, $this->DB_USER, $this->DB_PASS, $this->DB_NAME);
        if (mysqli_connect_errno()) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            return NULL;
        }
        return $mysqli;
    }

}

?>
