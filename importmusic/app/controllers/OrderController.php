<?php

class OrderController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		try{
			$response = [
				'orders'  => [],
				'status_code' => 200,
			];

			$orders = Order::with('items','store')->get();
			foreach($orders as $order){
				$response['orders'][] = $this->getOrder($order);
			}
		}catch (Exception $e){
			$response['status_code'] = 400;
			Log::error('OrderControler@index: '.$e->getMessage());
		}finally{
			return Response::json($response);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$order = Order::find($id);

			$response = [
				'order'  => $this->getOrder($order),
				'status_code' => 200,
			];
		}catch (Exception $e){
			$response['status_code'] = 404;
		}finally{
			return Response::json($response);
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		try{
			$response = [
				'status_code' => 200,
			];

			DB::transaction(function()
			{
				$this->createOrder();
			});
		}catch (Exception $e){
			$response['status_code'] = 400;
			$response['error']= $e->getMessage();
			Log::info('OrderControler@store: '.$e->getMessage());
		}finally{
			return Response::json($response);
		}
	}

	private function getOrder($order){
		$store = $order->store;
		$items = $this->getItems($order);

		return  [
			'id' => $order->id,
			'store' => $store,
			'country' => $order->country,
			'city' => $order->city,
			'total' => $order->quantity,
			'created_at' => $order->created_at,
			'total_price' => $order->total,
			'total_quantity' => $order->quantity,
			'items' => $items,
		];
	}

	private function getItems($order){
		$queryItems = $order->items;
		$items =[];

		foreach($queryItems as $item){
			$items[]=[
				'id'=> $item->id,
				'author'=>$item->author,
				'name'=>$item->name,
				'genre'=>$item->genre,
				'type'=>$item->type,
				'quantity'=>$item->pivot->quantity,
				'price'=>$item->pivot->price
			];
		}

		return $items;
	}

	private function createOrder(){
		$order = new Order;

		//get store name
		$store_name = Input::get('store');
		$store = Store::where('name','=',$store_name)->get()->first();

		//if the store does not exists create it
		if($store==null){
			$store= new Store;
			$store->name=$store_name;
			$store->save();
		}

		//get country
		$country = Input::get('country');
		$order->country=$country;

		//get city
		$city = Input::get('city');
		$order->city=$city;

		//get total
		$total = Input::get('total');
		$order->total=$total;

		//get quantity
		$quantity=Input::get('quantity');
		$order->quantity=$quantity;

		$store->hasMany('Store')->save($order);

		//getItems
		$this->createItems($order);

	}

	private function createItems($order){
		$inputItems = Input::get('items');

		foreach ($inputItems as $inputItem) {
			$item = Item::where('name', '=', $inputItem['name'])->get()->first();

			if ($item == null) {
				$item = new Item;
				$author = Author::where('name', '=', $inputItem['author'])->get()->first();

				if ($author == null) {
					$author = new Author;
					$author->name = $inputItem['author'];
					$author->save();
				}

				$item->name=$inputItem['name'];
				$item->genre = $inputItem['genre'];
				$item->type = $inputItem['type'];
				$author->items()->save($item);
			}

			$order->items()->attach($item, array(
				'quantity' => $inputItem['quantity'],
				'price' => $inputItem['price'],
			));
		}
	}
}
