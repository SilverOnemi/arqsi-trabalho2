<?php
/**
 * Created by IntelliJ IDEA.
 * User: silverone
 * Date: 11/27/14
 * Time: 9:40 PM
 */

class Store extends Eloquent{
    public $timestamps = false;

    protected $fillable = array('name');

    public function orders(){
        return $this->hasMany('Order');
    }
}