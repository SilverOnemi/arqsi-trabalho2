<?php
/**
 * Created by IntelliJ IDEA.
 * User: silverone
 * Date: 11/27/14
 * Time: 9:36 PM
 */

class Author extends Eloquent{
    public $timestamps = false;

    protected $fillable = array('name');

    public function items(){
        return $this->hasMany('Item');
    }
}