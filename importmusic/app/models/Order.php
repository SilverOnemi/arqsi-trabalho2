<?php
/**
 * Created by IntelliJ IDEA.
 * User: silverone
 * Date: 11/27/14
 * Time: 9:42 PM
 */

class Order extends Eloquent{
    protected $fillable = array('store_id','country',
        'city','address','quantity','total');

    public function items(){
        return $this->belongsToMany('Item')->withPivot('quantity','price');
    }

    public function store(){
        return $this->belongsTo('Store');
    }
}