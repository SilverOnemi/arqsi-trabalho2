<?php
/**
 * Created by IntelliJ IDEA.
 * User: silverone
 * Date: 11/27/14
 * Time: 9:37 PM
 */

class Item extends Eloquent{
    protected $dates = ['deleted_at'];

    protected $fillable= array('author_id','name',
                                'genre','type');

    public function author(){
        return $this->belongsTo('Author');
    }
}