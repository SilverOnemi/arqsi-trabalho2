<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('store_id')->unsigned();
			$table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade')->onUpdate('cascade');
			$table->string('country');
			$table->string('city');
			$table->decimal('total',8,2);
			$table->smallInteger('quantity')->unsigned();
			$table->timestamps();
		});

		Schema::create('item_order', function(Blueprint $table)
		{
			$table->integer('order_id')->unsigned();
			$table->integer('item_id')->unsigned();
			$table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('item_id')->references('id')->on('items')->onDelete('cascade')->onUpdate('cascade');

			$table->unique(array('order_id','item_id'));
			$table->decimal('price',8,2)->unsigned();
			$table->smallInteger('quantity')->unsigned()->default(1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('item_order');
		Schema::drop('orders');
	}

}
