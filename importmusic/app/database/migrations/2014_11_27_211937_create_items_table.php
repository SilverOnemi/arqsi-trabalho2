<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('items', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('author_id')->unsigned();
			$table->foreign('author_id')->references('id')->on('authors')->onDelete('cascade')->onUpdate('cascade');
			$table->string('name')->unique();
			$table->enum('genre', array('Avant-Garde', 'Blues',
				'Children', 'Classical', 'Country',
				'Easy Listening', 'Electronic', 'Folk',
				'Holiday', 'International', 'Jazz',
				'Latin', 'Rap', 'Reggae', 'Religious',
				'Vocal', 'Comedy', 'Trance',
				'Techno', 'Ambient', 'Breakbeat',
				'Chiptune', 'Disco', 'House',
				'Pop', 'Rock'
			));
			$table->enum('type', array('CD','Vinyl'));
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('items');
	}

}
