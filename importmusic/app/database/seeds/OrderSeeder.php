<?php
/**
 * Created by IntelliJ IDEA.
 * User: silverone
 * Date: 11/27/14
 * Time: 10:31 PM
 */

class OrderSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $order = new Order;
        $order->store_id=1;
        $order->country='Portugal';
        $order->city='Porto';
        $order->quantity=1;

        $item1 =Item::find(1);

        $order->total=5;
        $order->save();

        $order->items()->attach($item1, array('quantity'=>1, 'price'=>5));

        //saves the order and all its relations
        $order->push();
    }
}
