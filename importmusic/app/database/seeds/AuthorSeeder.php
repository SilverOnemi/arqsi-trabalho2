<?php
/**
 * Created by IntelliJ IDEA.
 * User: silverone
 * Date: 11/27/14
 * Time: 10:27 PM
 */

class AuthorSeeder  extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $author = new Author;
        $author->name='Madonna';
        $author->save();
    }
}