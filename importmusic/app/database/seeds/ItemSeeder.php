<?php
/**
 * Created by IntelliJ IDEA.
 * User: silverone
 * Date: 11/27/14
 * Time: 10:29 PM
 */

class ItemSeeder extends seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $item = new Item;
        $item->author_id=Author::where('name','=','Madonna')->firstOrFail()->id;
        $item->name='Like a virgin';
        $item->genre='pop';
        $item->type='Vinyl';
        $item->save();
    }
}