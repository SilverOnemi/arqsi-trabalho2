<?php
/**
 * Created by IntelliJ IDEA.
 * User: silverone
 * Date: 11/27/14
 * Time: 10:28 PM
 */

class StoreSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $store = new  Store;
        $store->name='2DSound';
        $store->save();
    }
}
