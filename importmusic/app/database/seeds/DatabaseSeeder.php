<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// $this->call('UserTableSeeder');
		$this->call('AuthorSeeder');
		$this->call('StoreSeeder');
		$this->call('ItemSeeder');
		$this->call('OrderSeeder');

	}

}
